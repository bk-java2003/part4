package com.itqf.controller;

import com.itqf.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/11
 * @Time: 下午2:56
 */
//@RestController
@Controller
public class MyController {

    @RequestMapping("/test")
    public  String testThymeleaf(Model model){
        model.addAttribute("name","thymeleaf入门");

        User user = new User();
        user.setId(1);
        user.setName("张三");
        user.setSex("男");
        model.addAttribute("user",user);

        List list = new ArrayList();
        for (int i = 0; i < 10; i++) {
            user = new User();
            user.setId(i+1);
            user.setName("张三"+i);
            user.setSex("男");
            list.add(user);
        }
        model.addAttribute("users",list);

        return  "index.html";
    }

}
