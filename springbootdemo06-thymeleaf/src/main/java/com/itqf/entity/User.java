package com.itqf.entity;

import lombok.Data;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/11
 * @Time: 下午3:06
 */
@Data
public class User {
    private  int id;
    private  String name;
    private  String sex;

}
