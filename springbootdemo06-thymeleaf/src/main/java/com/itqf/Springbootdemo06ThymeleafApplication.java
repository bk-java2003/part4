package com.itqf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springbootdemo06ThymeleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springbootdemo06ThymeleafApplication.class, args);
    }

}
