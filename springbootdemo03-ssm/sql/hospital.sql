/*
 Navicat Premium Data Transfer

 Source Server         : sawdqwdq
 Source Server Type    : MySQL
 Source Server Version : 50713
 Source Host           : localhost:3306
 Source Schema         : hospital

 Target Server Type    : MySQL
 Target Server Version : 50713
 File Encoding         : 65001

 Date: 26/02/2021 15:27:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cashier
-- ----------------------------
DROP TABLE IF EXISTS `cashier`;
CREATE TABLE `cashier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `register_id` int(11) NOT NULL COMMENT '挂号id',
  `name` varchar(500) NOT NULL COMMENT '药物名称或者项目名称',
  `num` int(11) NOT NULL COMMENT '数量',
  `price` double NOT NULL DEFAULT '0' COMMENT '价格',
  `total_price` double NOT NULL DEFAULT '0' COMMENT '小计',
  `type` int(1) DEFAULT NULL COMMENT '类型,0：挂号费 1:药 2:项目',
  `state` int(1) DEFAULT NULL COMMENT '状态,0:未缴费，1:缴费，2:项目已做',
  `ctime` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='收银记录表';

-- ----------------------------
-- Records of cashier
-- ----------------------------
BEGIN;
INSERT INTO `cashier` VALUES (1, 32, '板蓝根', 1, 30, 30, 1, 0, '2021-01-04');
INSERT INTO `cashier` VALUES (2, 33, '板蓝根', 2, 30, 60, 1, 1, '2020-02-04');
INSERT INTO `cashier` VALUES (3, 34, '板蓝根', 4, 30, 120, 1, NULL, '2019-02-03');
INSERT INTO `cashier` VALUES (4, 36, '板蓝根', 1, 30, 30, 1, 1, '2021-02-04');
INSERT INTO `cashier` VALUES (5, 36, '阿莫西林', 2, 30, 60, 1, 1, '2021-02-04');
INSERT INTO `cashier` VALUES (6, 37, '板蓝根', 1, 30, 30, 1, 1, '2021-02-04');
INSERT INTO `cashier` VALUES (7, 37, '阿莫西林', 2, 30, 60, 1, 1, '2021-02-04');
INSERT INTO `cashier` VALUES (8, 38, '板蓝根', 1, 30, 30, 1, 0, '2021-02-04');
INSERT INTO `cashier` VALUES (9, 38, '阿莫西林', 1, 30, 30, 1, 0, '2021-02-04');
INSERT INTO `cashier` VALUES (10, 39, '板蓝根', 2, 30, 30, 1, 0, '2021-02-05');
INSERT INTO `cashier` VALUES (11, 39, '阿莫西林', 2, 30, 60, 1, 0, '2021-02-05');
INSERT INTO `cashier` VALUES (12, 40, '板蓝根', 2, 30, 60, 1, 1, '2021-02-24');
INSERT INTO `cashier` VALUES (13, 40, '阿莫西林', 1, 30, 30, 1, 1, '2021-02-24');
COMMIT;

-- ----------------------------
-- Table structure for departments
-- ----------------------------
DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (
  `department_id` int(11) NOT NULL AUTO_INCREMENT,
  `department` varchar(300) NOT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='科室表';

-- ----------------------------
-- Records of departments
-- ----------------------------
BEGIN;
INSERT INTO `departments` VALUES (7, '内科');
INSERT INTO `departments` VALUES (8, '外科');
COMMIT;

-- ----------------------------
-- Table structure for doctor
-- ----------------------------
DROP TABLE IF EXISTS `doctor`;
CREATE TABLE `doctor` (
  `doctor_id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_name` varchar(300) NOT NULL,
  `department_id` int(11) NOT NULL,
  `registered_id` int(11) NOT NULL,
  `dstate` int(11) DEFAULT NULL,
  `am_Start_Time` varchar(10) DEFAULT '8:00',
  `am_End_Time` varchar(10) DEFAULT '12:00',
  `pm_Start_Time` varchar(10) DEFAULT '14:00',
  `pm_End_Time` varchar(10) DEFAULT '18:00',
  PRIMARY KEY (`doctor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='医生表';

-- ----------------------------
-- Records of doctor
-- ----------------------------
BEGIN;
INSERT INTO `doctor` VALUES (13, '华佗', 7, 5, 0, '8:00', '12:00', '14:00', '18:00');
INSERT INTO `doctor` VALUES (14, '扁鹊', 7, 6, 0, '8:00', '12:00', '14:00', '18:00');
INSERT INTO `doctor` VALUES (15, '张春明', 7, 5, 1, '8:00', '12:00', '14:00', '18:00');
INSERT INTO `doctor` VALUES (16, '王二', 8, 5, 1, '8:00', '12:00', '14:00', '18:00');
COMMIT;

-- ----------------------------
-- Table structure for paiban
-- ----------------------------
DROP TABLE IF EXISTS `paiban`;
CREATE TABLE `paiban` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `one` varchar(50) DEFAULT '无班',
  `two` varchar(50) DEFAULT '无班',
  `three` varchar(50) DEFAULT '无班',
  `four` varchar(50) DEFAULT '无班',
  `five` varchar(50) DEFAULT '无班',
  `six` varchar(50) DEFAULT '无班',
  `seven` varchar(50) DEFAULT '无班',
  `doctor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='排班表';

-- ----------------------------
-- Records of paiban
-- ----------------------------
BEGIN;
INSERT INTO `paiban` VALUES (12, '有班', '有班', '有班', '有班', '有班', '有班', '有班', 16);
INSERT INTO `paiban` VALUES (13, 'no', '有班', '有班', '有班', '有班', '有班', '有班', 15);
INSERT INTO `paiban` VALUES (14, '有班', '有班', '有班', '有班', '有班', '有班', '有班', 14);
INSERT INTO `paiban` VALUES (15, '有班', '有班', '有班', '无班', '有班', '有班', '有班', 13);
COMMIT;

-- ----------------------------
-- Table structure for pharmacy
-- ----------------------------
DROP TABLE IF EXISTS `pharmacy`;
CREATE TABLE `pharmacy` (
  `pharmacy_Id` int(11) NOT NULL AUTO_INCREMENT,
  `pharmacy_name` varchar(50) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `selling_price` double NOT NULL,
  `c_id` int(11) NOT NULL COMMENT '药品生产公司编号',
  `type` int(11) NOT NULL,
  `produceDate` date NOT NULL,
  `validDate` date NOT NULL,
  `drugstore_num` int(11) NOT NULL,
  `skullbatch` varchar(200) NOT NULL,
  PRIMARY KEY (`pharmacy_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='处方表';

-- ----------------------------
-- Records of pharmacy
-- ----------------------------
BEGIN;
INSERT INTO `pharmacy` VALUES (11, '板蓝根', 4, 30, 4, 5, '2019-10-30', '2020-01-04', 54, '20191121');
INSERT INTO `pharmacy` VALUES (12, '阿莫西林', 4, 30, 4, 5, '2019-10-25', '2020-02-01', 9, '20191121');
COMMIT;

-- ----------------------------
-- Table structure for register
-- ----------------------------
DROP TABLE IF EXISTS `register`;
CREATE TABLE `register` (
  `register_id` int(11) NOT NULL AUTO_INCREMENT,
  `register_name` varchar(100) DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `register_type_id` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `phone` varchar(100) NOT NULL,
  `carid` varchar(100) NOT NULL,
  `zhuan` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`register_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COMMENT='挂号信息表';

-- ----------------------------
-- Records of register
-- ----------------------------
BEGIN;
INSERT INTO `register` VALUES (32, 'zhangsan', '男', 90, 7, 13, 5, 20, '2021-01-03 17:17:17', 1, 2, '18192133232', '213132432423131', '病情加重');
INSERT INTO `register` VALUES (33, 'lisi', '男', 11, 7, 13, 5, 20, '2020-02-03 08:21:26', 1, 2, '12112212112', '212212121213232', '感冒');
INSERT INTO `register` VALUES (34, 'kkk', '男', 23, 7, 13, 5, 20, '2019-02-03 08:21:26', 1, 2, '17192133232', '432123456789765432', NULL);
INSERT INTO `register` VALUES (35, 'zhaosi', '男', 99, 7, 14, 6, 30, '2021-02-03 16:21:11', 1, 2, '19828837233', '213132432423132', NULL);
INSERT INTO `register` VALUES (36, 'ceshi', '男', 111, 7, 13, 5, 20, '2021-02-04 14:52:09', 1, 2, '15683823456', '234566890123475651', '感冒');
INSERT INTO `register` VALUES (37, 'aaa', '男', 111, 7, 14, 6, 30, '2021-02-04 16:36:37', 1, 2, '11323242434', '222324354546565767', '感冒');
INSERT INTO `register` VALUES (38, 'qqq', '男', 111, 7, 13, 5, 20, '2021-02-04 22:42:51', 1, 2, '12456789098', '543213456709876543', '1qq');
INSERT INTO `register` VALUES (39, 'hhh', '男', 34, 7, 14, 6, 30, '2021-02-05 09:33:10', 1, 2, '15432134576', '543213456787654321', 'ganmao');
INSERT INTO `register` VALUES (40, 'aaa', '男', 111, 7, 13, 5, 20, '2021-02-24 16:04:16', 1, 2, '16132324334', '132324354565657676', '发热');
COMMIT;

-- ----------------------------
-- Table structure for registered_type
-- ----------------------------
DROP TABLE IF EXISTS `registered_type`;
CREATE TABLE `registered_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(300) NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='挂号类型表';

-- ----------------------------
-- Records of registered_type
-- ----------------------------
BEGIN;
INSERT INTO `registered_type` VALUES (5, '普通挂号', 20);
INSERT INTO `registered_type` VALUES (6, '专家号', 30);
COMMIT;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `href` varchar(255) DEFAULT NULL,
  `spread` int(255) DEFAULT NULL COMMENT '0不展开1展开',
  `target` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `available` int(255) DEFAULT NULL COMMENT '0不可用1可用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_menu` VALUES (1, 0, '医院管理系统', '', 1, NULL, '&#xe68e;', 1);
INSERT INTO `sys_menu` VALUES (2, 1, '门诊管理', '', 0, NULL, '&#xe653;', 1);
INSERT INTO `sys_menu` VALUES (3, 1, '住院管理', '', 0, NULL, '&#xe663;', 1);
INSERT INTO `sys_menu` VALUES (4, 1, '系统管理', '', 0, '', '&#xe716;', 1);
INSERT INTO `sys_menu` VALUES (5, 1, '统计管理', '', 0, NULL, '&#xe629;', 1);
INSERT INTO `sys_menu` VALUES (6, 1, '数据中心', '', 0, NULL, '&#xe60a;', 1);
INSERT INTO `sys_menu` VALUES (7, 2, '用户挂号', '/cao/report.html', 0, NULL, '&#xe770;', 1);
INSERT INTO `sys_menu` VALUES (8, 2, '处方划价', '/cao/cashier.html', 0, NULL, '&#xe657;', 1);
INSERT INTO `sys_menu` VALUES (9, 2, '项目划价', '/cao/Cxiangmu.html', 0, NULL, '&#xe60a;', 1);
INSERT INTO `sys_menu` VALUES (10, 2, '项目缴费', '/cao/Cxiangmu.html', 0, NULL, '&#xe65e;', 1);
INSERT INTO `sys_menu` VALUES (11, 2, '项目检查', '/cao/Cxiangmu.html', 0, NULL, '&#xe674;', 1);
INSERT INTO `sys_menu` VALUES (12, 2, '药品缴费', '/cao/Ctoll.html', 0, NULL, '&#xe65e;', 1);
INSERT INTO `sys_menu` VALUES (13, 2, '门诊患者库', '/cao/chuanzhe.html', 0, NULL, '&#xe66f;', 1);
INSERT INTO `sys_menu` VALUES (14, 3, '入院登记', '/liao/admin.html', 0, NULL, '&#xe65b;', 1);
INSERT INTO `sys_menu` VALUES (15, 3, '缴费管理', '/liao/pay.html', 0, NULL, '&#xe6b2;', 1);
INSERT INTO `sys_menu` VALUES (16, 3, '项目记账', '/liao/item.html', 0, NULL, '&#xe705;', 1);
INSERT INTO `sys_menu` VALUES (17, 3, '出院结算', '/liao/leave.html', 0, NULL, '&#xe65a;', 1);
INSERT INTO `sys_menu` VALUES (18, 3, '药品记账', '/liao/drug.html', 0, NULL, '&#xe705;', 1);
INSERT INTO `sys_menu` VALUES (19, 4, '菜单管理', '/view/menu/menuManager.html', 0, NULL, '&#xe60f;', 1);
INSERT INTO `sys_menu` VALUES (20, 4, '角色管理', '/view/role/roleManager.html', 0, '', '&#xe66f;', 1);
INSERT INTO `sys_menu` VALUES (21, 4, '用户管理', '/view/user/userManager.html', 0, NULL, '&#xe770;', 1);
INSERT INTO `sys_menu` VALUES (22, 4, '数据源监控', '/druid/index.html', 0, NULL, '&#xe857;', 1);
INSERT INTO `sys_menu` VALUES (23, 5, '门诊月度统计', '/view/finance/reportManage.html', 0, NULL, '&#xe63c;', 1);
INSERT INTO `sys_menu` VALUES (24, 5, '住院月度统计', '/view/finance/zhuYuanManage.html', 0, NULL, '&#xe62c;', 1);
INSERT INTO `sys_menu` VALUES (25, 5, '门诊年度统计', '/view/finance/reportBing.html', 0, NULL, '&#xe62d;', 1);
INSERT INTO `sys_menu` VALUES (26, 5, '门诊当天统计', '/view/finance/current.html', 0, NULL, '&#xe60e;', 1);
INSERT INTO `sys_menu` VALUES (27, 5, '住院年度统计', '/view/finance/zhuYuanBing.html', 0, NULL, '&#xe630;', 1);
INSERT INTO `sys_menu` VALUES (28, 5, '医生统计对比', '/view/finance/doctorDuibi.html', 0, NULL, '&#xe770;', 1);
INSERT INTO `sys_menu` VALUES (29, 6, '科室中心', '/view/center/departments.html', 0, NULL, '&#xe68e;', 1);
INSERT INTO `sys_menu` VALUES (30, 6, '医生列表', '/view/center/doctor.html', 0, NULL, '&#xe66f;', 1);
INSERT INTO `sys_menu` VALUES (31, 6, '供货商', '/view/center/supply.html', 0, NULL, '&#xe613;', 1);
INSERT INTO `sys_menu` VALUES (33, 1, 'cash', '', 0, NULL, 'dowdy', 1);
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `roleid` int(11) NOT NULL AUTO_INCREMENT,
  `rolename` varchar(255) DEFAULT NULL,
  `roledesc` varchar(255) DEFAULT NULL,
  `available` int(11) DEFAULT NULL,
  PRIMARY KEY (`roleid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` VALUES (1, '超级管理员', '拥有所有菜单权限', 1);
INSERT INTO `sys_role` VALUES (2, '测试', '测试', 1);
INSERT INTO `sys_role` VALUES (4, '门诊管理', '拥有门诊管理的权限', 1);
COMMIT;

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `rid` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  PRIMARY KEY (`rid`,`mid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_menu` VALUES (1, 1);
INSERT INTO `sys_role_menu` VALUES (1, 2);
INSERT INTO `sys_role_menu` VALUES (1, 3);
INSERT INTO `sys_role_menu` VALUES (1, 4);
INSERT INTO `sys_role_menu` VALUES (1, 5);
INSERT INTO `sys_role_menu` VALUES (1, 6);
INSERT INTO `sys_role_menu` VALUES (1, 7);
INSERT INTO `sys_role_menu` VALUES (1, 8);
INSERT INTO `sys_role_menu` VALUES (1, 9);
INSERT INTO `sys_role_menu` VALUES (1, 10);
INSERT INTO `sys_role_menu` VALUES (1, 11);
INSERT INTO `sys_role_menu` VALUES (1, 12);
INSERT INTO `sys_role_menu` VALUES (1, 13);
INSERT INTO `sys_role_menu` VALUES (1, 14);
INSERT INTO `sys_role_menu` VALUES (1, 15);
INSERT INTO `sys_role_menu` VALUES (1, 17);
INSERT INTO `sys_role_menu` VALUES (1, 18);
INSERT INTO `sys_role_menu` VALUES (1, 19);
INSERT INTO `sys_role_menu` VALUES (1, 20);
INSERT INTO `sys_role_menu` VALUES (1, 21);
INSERT INTO `sys_role_menu` VALUES (1, 22);
INSERT INTO `sys_role_menu` VALUES (1, 23);
INSERT INTO `sys_role_menu` VALUES (1, 24);
INSERT INTO `sys_role_menu` VALUES (1, 25);
INSERT INTO `sys_role_menu` VALUES (1, 26);
INSERT INTO `sys_role_menu` VALUES (1, 27);
INSERT INTO `sys_role_menu` VALUES (1, 28);
INSERT INTO `sys_role_menu` VALUES (1, 29);
INSERT INTO `sys_role_menu` VALUES (1, 30);
INSERT INTO `sys_role_menu` VALUES (1, 31);
INSERT INTO `sys_role_menu` VALUES (4, 2);
INSERT INTO `sys_role_menu` VALUES (4, 7);
INSERT INTO `sys_role_menu` VALUES (4, 8);
INSERT INTO `sys_role_menu` VALUES (4, 9);
INSERT INTO `sys_role_menu` VALUES (4, 10);
INSERT INTO `sys_role_menu` VALUES (4, 11);
INSERT INTO `sys_role_menu` VALUES (4, 12);
INSERT INTO `sys_role_menu` VALUES (4, 13);
COMMIT;

-- ----------------------------
-- Table structure for sys_role_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user` (
  `uid` int(11) NOT NULL,
  `rid` int(11) NOT NULL,
  PRIMARY KEY (`uid`,`rid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_role_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_user` VALUES (1, 1);
INSERT INTO `sys_role_user` VALUES (1, 2);
INSERT INTO `sys_role_user` VALUES (1, 4);
INSERT INTO `sys_role_user` VALUES (2, 4);
COMMIT;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `loginname` varchar(255) DEFAULT NULL,
  `identity` varchar(255) DEFAULT NULL,
  `realname` varchar(255) DEFAULT NULL,
  `sex` int(255) DEFAULT NULL COMMENT '0女1男',
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `type` int(255) DEFAULT '2' COMMENT '1，超级管理员,2，系统用户',
  `available` int(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` VALUES (1, 'admin', '412365199001236544', '超级管理员', 0, '北京', '13183380741', '6af4d08340b548cbcce38353d9bcaab4', 'CEO', 1, 1, 'd21fd4');
INSERT INTO `sys_user` VALUES (2, 'mz', '412827199807156565', '门诊', 1, '河南', '13183365365', '3f5d2991001a3027712b17c831d524ad', '门诊管理员', 2, 1, '3c6226');
INSERT INTO `sys_user` VALUES (3, 'zhaorenhe', '121132323232323333', 'zhaorenhe', 1, NULL, '12323323232', '0def7c4382d43c742fbe14b46e27f342', '333', 2, NULL, '7a95ed');
INSERT INTO `sys_user` VALUES (4, NULL, NULL, NULL, NULL, NULL, NULL, '00046c613af3fde1fbd2b03589aa0f7e', NULL, 2, NULL, '323672');
COMMIT;

-- ----------------------------
-- Table structure for unit
-- ----------------------------
DROP TABLE IF EXISTS `unit`;
CREATE TABLE `unit` (
  `unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_name` varchar(50) NOT NULL,
  PRIMARY KEY (`unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='单位表';

-- ----------------------------
-- Records of unit
-- ----------------------------
BEGIN;
INSERT INTO `unit` VALUES (4, '盒');
INSERT INTO `unit` VALUES (8, '次');
INSERT INTO `unit` VALUES (9, '袋');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
