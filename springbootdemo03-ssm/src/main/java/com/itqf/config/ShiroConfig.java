package com.itqf.config;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/9
 * @Time: 下午2:23
 */
//xml方式
//@Configuration
//@ImportResource(locations = "classpath:spring/spring-shiro.xml")
//public class ShiroConfig {
//}

//注解方式  java对象代替xml

//@Configuration

import com.itqf.realm.MyUsersRealm;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;

import java.util.Map;

@SpringBootConfiguration  //配置文件
public  class ShiroConfig{
    /**
     * 1.构造DefaultWebSecurityManager
     *  <bean id="securityManager" class="org.apache.shiro.web.mgt.DefaultWebSecurityManager">
     *             <property name="realm" ref="myUserRealm"></property>
     *         </bean>
     */
    @Bean("securityManager")
    public DefaultWebSecurityManager defaultWebSecurityManager(MyUsersRealm realm){
        DefaultWebSecurityManager manager = new DefaultWebSecurityManager();
        manager.setRealm(realm);
        return  manager;
    }

    /**
     *         <bean id="myUserRealm" class="com.itqf.realm.MyUsersRealm">
     *             <!--设置密码比较器-->
     *             <property name="credentialsMatcher">
     *                 <bean class="org.apache.shiro.authc.credential.HashedCredentialsMatcher">
     *                     <!--加密方式-->
     *                     <property name="hashAlgorithmName" value="MD5"></property>
     *                     <!--迭代次数-->
     *                     <property name="hashIterations" value="2"></property>
     *                 </bean>
     *             </property>
     *         </bean>
     */
    @Bean
    public   MyUsersRealm myUsersRealm(){
        MyUsersRealm realm = new MyUsersRealm();
        HashedCredentialsMatcher credentialsMatcher = new HashedCredentialsMatcher();
        credentialsMatcher.setHashIterations(2);
        credentialsMatcher.setHashAlgorithmName("MD5");
        realm.setCredentialsMatcher(credentialsMatcher);

        return  realm;
    }

    /**
     * <bean id="shiroFilter" class="org.apache.shiro.spring.web.ShiroFilterFactoryBean">
     *             <property name="securityManager" ref="securityManager"></property>
     *             <property name="loginUrl" value="/view/login.html"></property>
     *             <property name="filterChainDefinitions">
     *                 <value>
     *
     *                     #放行静态资源
     *                     /static/** = anon
     *                     #放行登录请求
     *                     /sel/doLogin = anon
     *                     #其他请求必须登录后才能访问
     *                     /** = authc
     *                 </value>
     *             </property>
     *
     *         </bean>
     */

    @Bean  //构建对象
    public ShiroFilterFactoryBean shiroFilterFactoryBean(DefaultWebSecurityManager securityManager){
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager);
        shiroFilterFactoryBean.setLoginUrl("/view/login.html");
        Map<String,String> map =  shiroFilterFactoryBean.getFilterChainDefinitionMap();
        //LinkedHashMap  存取有序
        //hashMap 存取无序
        map.put("/static/**","anon");//静态资源放行
        map.put("/sel/doLogin","anon");//处理登录的请求放行
        map.put("/swagger-ui.html","anon");//在线文档放行
        map.put("/finance/doctorDuibi","anon");//在线文档放行

        map.put("/**","authc");//其他请求必须登录后才能访问

        return  shiroFilterFactoryBean;

    }


}