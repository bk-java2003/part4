package com.itqf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itqf.entity.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/1
 * @Time: 下午2:51
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     * 查询目录  menu.pid=1
     * @param uid
     * @return
     */
    public List<SysMenu> findDirByUId(@Param("uid") int uid);

    /**
     * 菜单  menu.pid!=1
     * @param uid  用户编号
     * @param pid 父目录编号
     * @return
     */
    public List<SysMenu> findMenuByUId(@Param("uid") int uid, @Param("pid") int pid);


}
