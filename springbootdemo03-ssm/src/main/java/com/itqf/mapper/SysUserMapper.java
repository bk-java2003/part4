package com.itqf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itqf.entity.SysUsers;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/1
 * @Time: 上午10:24
 */

public interface SysUserMapper extends BaseMapper<SysUsers> {

    public List<Integer> findRoleIdsByUid(int userid);

    public  int saveUserRoles(@Param("userid") int uid, @Param("roleid") int rid);

}
