package com.itqf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itqf.entity.RegisteredType;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/3
 * @Time: 上午10:05
 */
public interface RegisteredTypeMapper extends BaseMapper<RegisteredType> {
}
