package com.itqf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itqf.entity.Cashier;
import org.apache.ibatis.annotations.Param;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/4
 * @Time: 上午11:08
 */
public interface CashierMapper extends BaseMapper<Cashier> {

    public  double findTotalMoney(@Param("registerId") int regId);

}
