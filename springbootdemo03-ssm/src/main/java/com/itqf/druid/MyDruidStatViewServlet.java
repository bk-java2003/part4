package com.itqf.druid;

import com.alibaba.druid.support.http.StatViewServlet;

import javax.servlet.annotation.WebServlet;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/9
 * @Time: 下午3:04
 */
@WebServlet("/druid/*")
public class MyDruidStatViewServlet extends StatViewServlet {

}
