package com.itqf.service;

import com.itqf.utils.R;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/2
 * @Time: 上午11:22
 */
public interface SysRoleService {

    public R findRoleList(int userid);

}
