package com.itqf.service.impl;

import com.alibaba.druid.util.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itqf.entity.Departments;
import com.itqf.mapper.DepartmentsMapper;
import com.itqf.service.DepartmentsService;
import com.itqf.utils.R;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/3
 * @Time: 上午10:10
 */
@Service
public class DepartmentsServiceImpl implements DepartmentsService {
    @Resource
    private DepartmentsMapper departmentsMapper;

    @Override
    public List<Departments> findAllDepartments() {
        return departmentsMapper.selectList(null);
    }

    @Override
    public R findByPage(int page, int limit, Departments departments) {
        IPage iPage = new Page(page,limit);
        if (departments!=null){
            QueryWrapper queryWrapper = new QueryWrapper();
            if (departments.getDepartmentId()!=null){
                queryWrapper.eq("department_id",departments.getDepartmentId());
            }

            if (!StringUtils.isEmpty(departments.getDepartment())){
                queryWrapper.like("department_",departments.getDepartment());
            }

            iPage    =  departmentsMapper.selectPage(iPage,queryWrapper);

        }else{
            iPage    =  departmentsMapper.selectPage(iPage,null);

        }

        return R.ok("").put("data",iPage.getRecords()).put("count",iPage.getTotal());

    }
}
