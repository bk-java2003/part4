package com.itqf.service.impl;

import com.itqf.entity.RegisteredType;
import com.itqf.mapper.RegisteredTypeMapper;
import com.itqf.service.RegisteredTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/3
 * @Time: 上午10:10
 */
@Service
public class RegisteredTypeServiceImpl implements RegisteredTypeService {
    @Resource
    private RegisteredTypeMapper registeredTypeMapper;

    @Override
    public List<RegisteredType> findAllRegType() {
        return registeredTypeMapper.selectList(null);
    }

    @Override
    public Double findMoney(int id) {
        RegisteredType registeredType= registeredTypeMapper.selectById(id);
        return registeredType.getPrice();
    }
}
