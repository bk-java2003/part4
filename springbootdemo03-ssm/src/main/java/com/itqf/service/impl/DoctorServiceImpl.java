package com.itqf.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itqf.entity.Doctor;
import com.itqf.mapper.DoctorMapper;
import com.itqf.service.DoctorService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/3
 * @Time: 上午10:13
 */
@Service
public class DoctorServiceImpl implements DoctorService {

    @Resource
    private DoctorMapper doctorMapper;

    @Override
    public List<Doctor> findDoctor(int regId, int depId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("registered_id",regId);//专家号还是普通号
        queryWrapper.eq("department_id",depId);//科室


        return doctorMapper.selectList(queryWrapper);
    }

    @Override
    public List<Doctor> findOneDoctor(String columnName) {
        return doctorMapper.findDoctorByColumn(columnName);
    }
}
