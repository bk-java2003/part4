package com.itqf.service.impl;

import com.itqf.dto.PharmacyDTO;
import com.itqf.mapper.PharmacyMapper;
import com.itqf.service.PharmacyService;
import com.itqf.utils.R;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/4
 * @Time: 上午10:04
 */
@Service
public class PharmacyServiceImpl implements PharmacyService {

    @Resource
    private PharmacyMapper pharmacyMapper;

    @Override
    public R findDrug(int page, int limit) {

        List<PharmacyDTO> list = pharmacyMapper.findDrug((page-1)*limit,limit);
        int   total = pharmacyMapper.findTotalCount();

        //{code:0,msg:"",data:[{}],"count":10}
        return R.ok("").put("data",list).put("count",total);
    }
}
