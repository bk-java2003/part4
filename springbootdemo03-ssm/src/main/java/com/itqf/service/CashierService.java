package com.itqf.service;

import com.itqf.entity.Cashier;
import com.itqf.utils.R;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/4
 * @Time: 上午11:09
 */
public interface CashierService {

    public R findChuByRegId(int regid, int page, int limit);

    public int  findChu(int regid, String name);

    public    R   add(Cashier cashier);
    public    R   update(Cashier cashier);
    public    R   del(int id);

    public  double  findTotalMoney(int regId);

    public    R   updateState(int id);

    public R findNotPayDrug(int regid, int page, int limit);


}
