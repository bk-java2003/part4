package com.itqf.service;

import com.itqf.dto.MenuTreeNode;
import com.itqf.entity.SysMenu;
import com.itqf.utils.R;

import java.util.List;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/1
 * @Time: 下午3:11
 */
public interface SysMenuService {


    /**
     * [
     * {
     * id: 2,
     * title: "门诊管理",
     * icon: "&#xe653;",
     * href: "",
     * spread: false,
     * target: null,
     * children: [
     * {
     * id: 7,
     * title: "用户挂号",
     * icon: "&#xe770;",
     * href: "/cao/report.html",
     * spread: false,
     * target: null,
     * children: [ ],
     * checkArr: "0",
     * parentId: 2}
     * @param uid
     * @return
     */
    public List<MenuTreeNode> leftTreeMenu(int uid);

    public R menuleftTreeJson();

    public R menuRight(int page, int limit, SysMenu menu);

    public  R  updateMenu(SysMenu sysMenu);
    public  R  addMenu(SysMenu sysMenu);
    public  R  deleteMenu(int id);
    public  R  checkMenuHasChildren(int id);



}
