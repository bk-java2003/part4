package com.itqf.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

public class Doctor implements Serializable {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column doctor.doctor_id
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    @TableId(value = "doctor_id",type = IdType.AUTO)
    private Integer doctorId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column doctor.doctor_name
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    private String doctorName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column doctor.department_id
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    private Integer departmentId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column doctor.registered_Id
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    private Integer registeredId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column doctor.dstate
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    private Integer dstate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column doctor.am_Start_Time
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    private String amStartTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column doctor.am_End_Time
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    private String amEndTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column doctor.pm_Start_Time
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    private String pmStartTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column doctor.pm_End_Time
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    private String pmEndTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table doctor
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column doctor.doctor_id
     *
     * @return the value of doctor.doctor_id
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    public Integer getDoctorId() {
        return doctorId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column doctor.doctor_id
     *
     * @param doctorId the value for doctor.doctor_id
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column doctor.doctor_name
     *
     * @return the value of doctor.doctor_name
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    public String getDoctorName() {
        return doctorName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column doctor.doctor_name
     *
     * @param doctorName the value for doctor.doctor_name
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName == null ? null : doctorName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column doctor.department_id
     *
     * @return the value of doctor.department_id
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    public Integer getDepartmentId() {
        return departmentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column doctor.department_id
     *
     * @param departmentId the value for doctor.department_id
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column doctor.registered_Id
     *
     * @return the value of doctor.registered_Id
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    public Integer getRegisteredId() {
        return registeredId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column doctor.registered_Id
     *
     * @param registeredId the value for doctor.registered_Id
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    public void setRegisteredId(Integer registeredId) {
        this.registeredId = registeredId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column doctor.dstate
     *
     * @return the value of doctor.dstate
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    public Integer getDstate() {
        return dstate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column doctor.dstate
     *
     * @param dstate the value for doctor.dstate
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    public void setDstate(Integer dstate) {
        this.dstate = dstate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column doctor.am_Start_Time
     *
     * @return the value of doctor.am_Start_Time
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    public String getAmStartTime() {
        return amStartTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column doctor.am_Start_Time
     *
     * @param amStartTime the value for doctor.am_Start_Time
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    public void setAmStartTime(String amStartTime) {
        this.amStartTime = amStartTime == null ? null : amStartTime.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column doctor.am_End_Time
     *
     * @return the value of doctor.am_End_Time
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    public String getAmEndTime() {
        return amEndTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column doctor.am_End_Time
     *
     * @param amEndTime the value for doctor.am_End_Time
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    public void setAmEndTime(String amEndTime) {
        this.amEndTime = amEndTime == null ? null : amEndTime.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column doctor.pm_Start_Time
     *
     * @return the value of doctor.pm_Start_Time
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    public String getPmStartTime() {
        return pmStartTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column doctor.pm_Start_Time
     *
     * @param pmStartTime the value for doctor.pm_Start_Time
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    public void setPmStartTime(String pmStartTime) {
        this.pmStartTime = pmStartTime == null ? null : pmStartTime.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column doctor.pm_End_Time
     *
     * @return the value of doctor.pm_End_Time
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    public String getPmEndTime() {
        return pmEndTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column doctor.pm_End_Time
     *
     * @param pmEndTime the value for doctor.pm_End_Time
     *
     * @mbggenerated Wed Feb 03 09:47:07 CST 2021
     */
    public void setPmEndTime(String pmEndTime) {
        this.pmEndTime = pmEndTime == null ? null : pmEndTime.trim();
    }
}