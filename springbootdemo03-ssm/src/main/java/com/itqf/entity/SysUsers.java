package com.itqf.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/1
 * @Time: 上午10:19
 */
@TableName("sys_user")
@Data
@ApiModel(value = "用户",description = "系统用户")
public class SysUsers {
    @ApiModelProperty(value = "用户编号",example = "1")
    @TableId(value = "userid",type = IdType.AUTO)
    private Integer userid;
    @ApiModelProperty(value = "用户名",name = "loginname",example = "admin")
    private  String  loginname;
    private  String  identity;
    private  String  realname;
    private  String  pwd;
    private  String  position;
    private  String  salt;

    private  String  phone;
    private Integer sex;
    private Integer type;

    private  Integer available;


}
