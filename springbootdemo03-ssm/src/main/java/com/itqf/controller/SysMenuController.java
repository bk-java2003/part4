package com.itqf.controller;

import com.itqf.entity.SysMenu;
import com.itqf.service.SysMenuService;
import com.itqf.utils.R;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/2
 * @Time: 下午3:12
 */
@RestController //@Controller+@ReponseBody
public class SysMenuController {

    @Resource
    private SysMenuService sysMenuService;

    @RequestMapping("/sel/loadMenuMangerLeftTreeJson")
    public R menuLeftTreeJson(){

        return  sysMenuService.menuleftTreeJson();
    }

    @RequestMapping("/sel/queryMenuAllList")
    public   R  rightMenuTable(int page, int limit, SysMenu menu){

        return  sysMenuService.menuRight(page,limit,menu);
    }


    @RequestMapping("/sel/updateMenu")
    public  R  update(SysMenu sysMenu){
        return  sysMenuService.updateMenu(sysMenu);
    }

    @RequestMapping("/sel/addMenu")
    public  R  addMenu(SysMenu sysMenu){
        return  sysMenuService.addMenu(sysMenu);
    }
    @RequestMapping("/sel/deleteMenu")
    public  R  delete(int id){
        return  sysMenuService.deleteMenu(id);
    }

    ///sel/checkMenuHasChildren?id=" + data.id
    @RequestMapping("/sel/checkMenuHasChildren")
    public   R  checkMenu(int id){

        return  sysMenuService.checkMenuHasChildren(id);
    }


}
