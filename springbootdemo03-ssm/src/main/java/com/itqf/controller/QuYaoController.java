package com.itqf.controller;

import com.itqf.entity.Cashier;
import com.itqf.service.CashierService;
import com.itqf.service.PharmacyService;
import com.itqf.service.RegisterService;
import com.itqf.utils.R;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/3
 * @Time: 下午4:26
 */
@RestController
public class QuYaoController {

    @Resource
    private RegisterService registerService;

    @Resource
    private PharmacyService pharmacyService;

    @Resource
    private CashierService cashierService;

    @RequestMapping("/caotake/tselpreson")
    //layui table
    /**
     * 查挂号的患者
     */
    public R  selPerson(int page, int limit){
        return registerService.findByPage(page,limit);
    }

    /**
     * 查所有药物
     * @param page
     * @param limit
     * @return
     */
    @RequestMapping("/caotake/selDrug")
    public   R  selectDrug(int page,int  limit){
        return pharmacyService.findDrug(page,limit);
    }

    /**
     * 查询该用户的处方记录
     * @param perid
     * @return
     */
    @RequestMapping("/caocc/selAll")//?perid='+perid
    public   R  findAllHasDrug(int perid,int page,int limit){

        return  cashierService.findChuByRegId(perid,page,limit);
    }

    /**
     * 判断处方中有没有该药物
     * @param reid  挂号id
     * @param mename 药名称
     * @return
     */
    @RequestMapping("/caocc/selchu")//{"reid":reid,"mename":mename}
    public  Object  selChu(int  reid,String mename){

        return  cashierService.findChu(reid,mename);
    }
    /**
     * 添加处方

     * @return
     */
    @RequestMapping("/caocc/addchu")
    public   R  addChu(Cashier cashier){
        cashier.setType(1);//0挂号，1 药  2项目（CT  心电图）
        cashier.setState(0);//0:未缴费，1:缴费 ，2:项目已做'
        cashier.setCtime(new Date());
        return  cashierService.add(cashier);
    }

    @RequestMapping("/caocc/updchu")
    public   R  updChu(Cashier cashier){
        return  cashierService.update(cashier);
    }

    @RequestMapping("/caocc/selbing")
    public String   selReason(int reid){

        return  registerService.findReason(reid);
    }

    @RequestMapping("/caocc/addbing")///caocc/addbing",{"reid":reid,"bing":bing}
    public R  addReason(int regid,String bing){

        return  registerService.addReason(regid,bing);
    }
    /**
     * 删除处方

     * @return
     */
    @RequestMapping("/caocc/del")
    public   R del(int id){
        return  cashierService.del(id);
    }

    /**
     * 查询患者
     * @return
     */
    @RequestMapping("/caocc/selpreson")
    public   R  selPerson1(int  page,int limit){

        return  registerService.findByPage(page,limit);
    }

    /**查询未缴费的药物信息
     * /caocc/selNotPayDrug?perid='+perid
     */
    @RequestMapping("/caocc/selNotPayDrug")
    public  R  selectDrug(int perid,int page,int limit){
        return  cashierService.findNotPayDrug(perid,page,limit);
    }

    /**
     * 查询处方总的金额
     * @param regId
     * @return
     */
    @RequestMapping("/caoout/selch")
    public   Object findTotalMoney(int regId){

        return cashierService.findTotalMoney(regId);
    }

    @RequestMapping("/caoout/shoufei")
    public  R  shoufei(int registerId){//{register:reid}
        //			'状态,0:未缴费，1:缴费 ，2:项目已做
        return  cashierService.updateState(registerId);
    }


}
