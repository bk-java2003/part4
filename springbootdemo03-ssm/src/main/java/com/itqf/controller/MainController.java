package com.itqf.controller;

import com.itqf.entity.Doctor;
import com.itqf.service.DoctorService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/5
 * @Time: 下午3:54
 */
@RestController
@RequestMapping("/main")
public class MainController {

    @Resource
    private DoctorService doctorService;

    @RequestMapping("/one")
    public List<Doctor> findOne(){

        return  doctorService.findOneDoctor("one");
    }
    @RequestMapping("/two")
    public List<Doctor> findTwo(){
        return  doctorService.findOneDoctor("two");
    }
    @RequestMapping("/three")
    public List<Doctor> findThree(){
        return  doctorService.findOneDoctor("three");
    }
    @RequestMapping("/four")
    public List<Doctor> findFour(){
        return  doctorService.findOneDoctor("four");

    }
    @RequestMapping("/five")
    public List<Doctor> findFive(){
        return  doctorService.findOneDoctor("five");

    }
    @RequestMapping("/six")
    public List<Doctor> findSix(){
        return  doctorService.findOneDoctor("six");

    }
    @RequestMapping("/seven")
    public List<Doctor> findSeven(){
        return  doctorService.findOneDoctor("seven");

    }



}
