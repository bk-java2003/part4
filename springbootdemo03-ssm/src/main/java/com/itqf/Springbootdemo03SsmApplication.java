package com.itqf;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication  //扫描同级和子包的spring容器的注解
//扫描servlet,filter的注解
@ServletComponentScan(basePackages = "com.itqf.druid")
//扫描mapper层
@MapperScan(basePackages = "com.itqf.mapper")
//<bean class=".......MapperScannerConfigurer">
// <property name="basePackage" value="com.itqf.mapper">
public class Springbootdemo03SsmApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springbootdemo03SsmApplication.class, args);
    }

}
