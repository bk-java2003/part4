package com.itqf.utils;

import com.itqf.entity.SysUsers;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.subject.Subject;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/1
 * @Time: 下午4:25
 */
public class ShiroUtils {

    /**
     *  Subject subject = SecurityUtils.getSubject();
     *         subject.getPrincipal();当前登录的用户
     */
    public  static SysUsers getCurrentUsers(){
        Subject subject = SecurityUtils.getSubject();
        //subject.getPrincipal() 得到的是认证时存储到shiro内部的用户对象
//        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo
//                (sysUsers,sysUsers.getPwd(),ByteSource.Util.bytes(sysUsers.getSalt()),this.getName());

        return (SysUsers) subject.getPrincipal();
    }

    public  static  int  getUid(){

        return  getCurrentUsers().getUserid();
    }

    public  static  String   getSalt(){

        SecureRandomNumberGenerator generator = new SecureRandomNumberGenerator();
        //一个字符=2byte
        //byte[] bytes = new byte[numBytes];
        return  generator.nextBytes(3).toHex();

    }

    public  static  String  getPwd(String source,String salt){

        return new Md5Hash(source,salt,2).toHex();
    }


}
