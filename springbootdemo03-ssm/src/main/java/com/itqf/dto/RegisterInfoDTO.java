package com.itqf.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @Description: 扩展类(register+doctor+departments+registeredType)
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/3
 * @Time: 下午2:48
 */
@Data
public class RegisterInfoDTO {

    private  Integer  registerId;
    private  String registerName;
    private  String phone;
    private  String carid;
    private  String   registeredType;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GTM+8")
    private Date datime;
    private  String  department;
    private  String  doctorName;

    private String  sex;
    private  String age;
    private double price;



}
