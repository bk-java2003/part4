package com.itqf.dto;

import com.itqf.entity.SysMenu;
import lombok.Data;

import java.util.List;

/**
 * @Description:  为了封装左侧菜单
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/1
 * @Time: 下午3:13
 */
@Data
public class MenuTreeNode extends SysMenu {
   //json---children: [{},{}]     {}
    //java---List<MenuTreeNode>    对象|Map

    private List<MenuTreeNode> children;
    // * checkArr: "0",
    //     * parentId: 2}
    private  String  checkArr;
    private int parentId;


    @Override
    public String toString() {
        return "MenuTreeNode{" +
                "children=" + children +
                ", checkArr='" + checkArr + '\'' +
                ", parentId=" + parentId +
                '}'+super.toString();
    }
}
