package com.itqf;

import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.util.UUID;

@SpringBootTest(classes = SpringbootMqProducterApplication.class)
@ContextConfiguration
class SpringbootMqProducterApplicationTests {

    //发消息
    @Resource
    private RabbitTemplate rabbitTemplate;
    //private AmqpTemplate amqpTemplate;

    @Test
    void contextLoads()  throws  Exception{

        //String routingKey, Object object
        //简单
       // amqpTemplate.convertAndSend("springboot-queue","hello,我的第一个springboot的入门案例！！！");

        //fanout模式
        //String exchange, String routingKey, Object object
        //amqpTemplate.convertAndSend("springboot-fanout-exchange","","hello,fanout模式的消息");

        //direct 模式  定向
        //String exchange, String routingKey, Object object

        //使用springboot实现confirm和消息回退写法二

        rabbitTemplate.setConfirmCallback(new RabbitTemplate.ConfirmCallback() {
            @Override
            public void confirm(CorrelationData correlationData, boolean b, String s) {
                if (b){
                    System.out.println("消息发送成功(p->mq)");
                }else{
                    System.out.println("消息发送失败(p->mq)");
                }
            }
        });

        rabbitTemplate.setReturnsCallback(new RabbitTemplate.ReturnsCallback() {
            @Override
            public void returnedMessage(ReturnedMessage returnedMessage) {
                System.out.println("消息回退:"+new String(returnedMessage.getMessage().getBody()));
                System.out.println(returnedMessage.getReplyText()+"--"+returnedMessage.getRoutingKey());
            }
        });


        //rabbitTemplate.convertAndSend("springboot-direct-exchange","info","info 级别级别的消息");

        String json = "error 级别级别的消息 测试重复消费";//要发送的消息
        //日志  视频的编号  商品的id
        Message message = MessageBuilder.withBody(json.getBytes())
                .setContentType("application/json").
                        setContentEncoding("utf-8")
                .setMessageId(UUID.randomUUID().toString().replace("-",""))
                .build();
        //djiwhdfiwfhwifhi-nfcskncfskfncskfnks-ml

        rabbitTemplate.convertAndSend("springboot-direct-exchange","error",message);


        //TimeUnit.SECONDS.sleep(5);
        System.in.read();
    }

}
