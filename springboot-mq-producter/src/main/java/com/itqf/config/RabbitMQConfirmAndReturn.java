package com.itqf.config;

import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/13
 * @Time: 上午10:05
 */
//@Component    使用springboot实现confirm和消息回退写法一
public class RabbitMQConfirmAndReturn  implements RabbitTemplate.ConfirmCallback,RabbitTemplate.ReturnsCallback {

    //channel.addConfirmLister();
    @Resource
    RabbitTemplate rabbitTemplate;

    @PostConstruct //初始化
    public  void  init(){
        System.out.println("初始化......");
        rabbitTemplate.setConfirmCallback(this);
        rabbitTemplate.setReturnsCallback(this::returnedMessage);
    }


    //@PreDestroy    //销毁




    @Override
    public void confirm(CorrelationData correlationData, boolean b, String s) {
        if (b){
            System.out.println("消息发送成功！"+correlationData+"---"+b+"---"+s);
        }else{
            System.out.println("消息发送失败！"+correlationData+"---"+b+"---"+s);
        }
    }

    @Override
    public void returnedMessage(ReturnedMessage returnedMessage) {
        System.out.println("消息回退：！"+returnedMessage.getMessage());
        System.out.println("消息回退：！"+returnedMessage.getExchange()+"--"+returnedMessage.getRoutingKey()
        +"---"+returnedMessage.getReplyText());


    }
}
