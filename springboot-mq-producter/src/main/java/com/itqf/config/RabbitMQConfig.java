package com.itqf.config;

import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/12
 * @Time: 下午2:40
 */
@Configuration  //xml <beans>  ioc容器
public class RabbitMQConfig {
// 简单模式  一个消费者一个发送者
    @Bean  //构建一个队列
    public Queue queue(){
        return  new Queue("springboot-queue");
    }

//    @Bean
//    public FanoutExchange fanoutExchange(){
//        //String name, boolean durable, boolean autoDelete
//        return  new FanoutExchange("springboot-fanout-exchange",false,false);
//    }

    //生产者 --交换机
    @Bean
    public DirectExchange  directExchange(){
        return  new DirectExchange("springboot-direct-exchange",false,false);
    }




}
