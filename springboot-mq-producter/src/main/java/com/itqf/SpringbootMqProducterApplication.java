package com.itqf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootMqProducterApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootMqProducterApplication.class, args);
    }

}
