package com.itqf.springbootdemo01.entity;

import lombok.Data;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/9
 * @Time: 上午10:13
 */
@Data
public class Doctor {

    private int doctorId;
    private String doctorName;


}
