package com.itqf.springbootdemo01.mapper;

import com.itqf.springbootdemo01.entity.Doctor;

import java.util.List;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/9
 * @Time: 上午10:15
 */
public interface DoctorMapper {


    public List<Doctor>  findAll();

}
