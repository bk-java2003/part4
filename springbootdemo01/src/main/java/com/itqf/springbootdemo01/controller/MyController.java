package com.itqf.springbootdemo01.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/9
 * @Time: 上午9:33
 */
@RestController
public class MyController {


    @RequestMapping("/test")
    public  String test(){
        System.out.println("hello,我的第一个Springboot项目");

        return  "hello,springboot!!!";
    }


}
