package com.itqf.springbootdemo01;

import com.itqf.springbootdemo01.mapper.DoctorMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;


@SpringBootTest(classes = Springbootdemo01Application.class)
@ContextConfiguration
class Springbootdemo01ApplicationTests {

    //注入mapper
    @Resource
    private DoctorMapper doctorMapper;

    @Test
    void contextLoads() {
        System.out.println(doctorMapper.findAll());
    }

}
