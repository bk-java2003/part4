package com.itqf.springbootdemo01;

import com.itqf.springbootdemo01.mapper.DoctorMapper;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;



//@RunWith(SpringJUnit4ClassRunner.class);
//@ContextConfiguration(locations = "")
class Springbootdemo01ApplicationTests1 {

    //注入mapper
    @Resource
    private DoctorMapper doctorMapper;

    @Test
    void contextLoads() {
        System.out.println(doctorMapper.findAll());
    }

}
