package com.itqf;

import com.itqf.entity.User;
import org.springframework.util.SerializationUtils;
import redis.clients.jedis.Jedis;

import java.util.Date;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/2
 * @Time: 下午4:17
 */
public class TestRedis2 {

    public  static  void  main(String[]args){
        //1.创建Jedis对象   ip  port
        Jedis jedis = new Jedis("192.168.82.168",6379);
        //2.测试
        System.out.println(jedis.ping());//PONG

        User user = new User(1,"admin",new Date());

        byte key[] = SerializationUtils.serialize("user");
        byte value[] = SerializationUtils.serialize(user);

        jedis.set(key,value);

        //取
        User user1 = (User) SerializationUtils.deserialize(jedis.get(key));
        System.out.println(user);

        jedis.close();

    }

}
