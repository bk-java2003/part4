package com.itqf;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Tuple;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/2
 * @Time: 下午3:59
 */
public class TestRedis {

    public  static  void  main(String[]args){
        //1.创建Jedis对象   ip  port
        Jedis jedis = new Jedis("192.168.82.168",7002);//master
        //NOAUTH Authentication required.
       // jedis.auth("jbgsn");
        //2.测试
        System.out.println(jedis.ping());//PONG

        //String
        jedis.set("username","admin");//set username admin
        System.out.println(jedis.get("username"));

        //hash
        Map<String,String> map = new HashMap<>();
        map.put("name","赵四");
        map.put("age","18");
        jedis.hmset("users",map);
        System.out.println(jedis.hgetAll("users"));

        //list
        jedis.lpush("list1","a","b","c","1");
        System.out.println(jedis.lrange("list1",0,-1));

        //set
        jedis.sadd("set1","1","1","2","3","4");
        System.out.println(jedis.smembers("set1"));
        System.out.println("中奖号码："+jedis.spop("set1"));

        //zset
        Map<String,Double> stringDoubleMap = new HashMap<>();
        stringDoubleMap.put("milk:1001",23902d);
        stringDoubleMap.put("candy:1001",232d);
        stringDoubleMap.put("apple:1001",43434.0);
        stringDoubleMap.put("orange:1001",902d);
        jedis.zadd("goods",stringDoubleMap);
        Set<Tuple> set= jedis.zrangeByScoreWithScores("goods",0,9999999);
        for (Tuple tuple : set) {
            System.out.println(tuple.getElement()+"---"+tuple.getScore());
        }

        System.out.println();

        //关闭连接
        jedis.close();






    }


}
