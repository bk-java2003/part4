package com.itqf;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/2
 * @Time: 下午4:24
 */
public class TestRedisPipeline {

    public  static  void  main(String[]args){
        //1.创建Jedis对象   ip  port
        Jedis jedis = new Jedis("192.168.82.168",6379);
        //2.测试
        System.out.println(jedis.ping());//PONG

        for (int i = 0; i < 100; i++) {
            jedis.incr("num");//发送100次命令到达redis-server  incr  num
        }


        System.out.println("--------");
       Pipeline pipeline = jedis.pipelined();

        for (int i = 0; i < 100; i++) {
            pipeline.incr("num");
        }

        pipeline.syncAndReturnAll();//发送一次
        System.out.println(jedis.get("num")); //1 次：incr num


        jedis.close();

    }

}
