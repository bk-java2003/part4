package com.itqf.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/2
 * @Time: 下午4:16
 */
@Data
@NoArgsConstructor  //无参构造方法
@AllArgsConstructor  //带全参构造方法
public class User implements Serializable {

    private Integer id;

    private String name;

    private Date birthday;

}