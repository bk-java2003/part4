package com.itqf;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

import java.util.HashSet;
import java.util.Set;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/4
 * @Time: 下午2:30
 */
public class TestRedisCluster {

    public  static  void  main(String[]args) throws  Exception{
        //3 master  3 slave
        //Set<HostAndPort> nodes
        Set<HostAndPort> set = new HashSet<>();
        set.add(new HostAndPort("192.168.82.168",7001));
        set.add(new HostAndPort("192.168.82.168",7002));
        set.add(new HostAndPort("192.168.82.168",7003));
        set.add(new HostAndPort("192.168.82.168",7004));
        set.add(new HostAndPort("192.168.82.168",7005));
        set.add(new HostAndPort("192.168.82.168",7006));

        JedisCluster cluster = new JedisCluster(set);

        System.out.println(cluster.set("name","admin"));
        System.out.println(cluster.get("name"));

        //hset  批量处理的命令有可能会报错
        //   because keys have different slots.
        //a  7003
        //b  7001
//        System.out.println(cluster.mset("a","aa","b","bb"));
//        System.out.println(cluster.mget("a","b"));

        cluster.close();

    }
}
