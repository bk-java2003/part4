package com.itqf;

import com.alibaba.fastjson.JSON;
import com.itqf.entity.User;
import redis.clients.jedis.Jedis;

import java.util.Date;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/2
 * @Time: 下午4:24
 */
public class TestRedisString {

    public  static  void  main(String[]args){
        //1.创建Jedis对象   ip  port
        Jedis jedis = new Jedis("192.168.82.168",6379);
        //2.测试
        System.out.println(jedis.ping());//PONG

        User user = new User(2,"尼古拉斯.赵四",new Date());

        String   value = JSON.toJSONString(user);

        jedis.set("stringUser",value);

        String   json = jedis.get("stringUser");
        System.out.println(json);

        System.out.println(JSON.parseObject(json,User.class));

        jedis.close();

    }

}
