package com.itqf;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itqf.entity.TbItem;
import com.itqf.mapper.TbItemMapper;
import com.itqf.service.SearchService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/15
 * @Time: 上午9:34
 */
@SpringBootTest(classes = SpringbootElsaticsearchApplication.class)
@ContextConfiguration
public class TestElasticSearch {//spring容器注入对象  必须加这些注解

    @Resource
    private TbItemMapper tbItemMapper;

    //RestHighLevelClient client = EsClient.client();
    @Resource
    private SearchService searchService;

    @Resource
    private ObjectMapper objectMapper;

    /**
     * PUT  /shop
     * {
     *   "settings": {
     *     "number_of_shards": 3
     *     , "number_of_replicas": 1
     *   },
     *   "mappings": {
     *     "book":{
     *       "properties":{
     *          "id":{
     *             "type":"integer",
     *             "store":true
     *          },
     *          "name":{
     *            "type":"text",
     *            "analyzer":"ik_max_word"
     *          },
     *          "author":{
     *            "type":"keyword"
     *          }
     *       }
     *     }
     *   }
     *
     * }
     */
    private  String  indexName = "shop";
    private  String  typeName = "tb_item";
    @Test
    public  void testCreateIndex() throws  Exception{
        System.out.println(indexName+"存在与否："+searchService.existsIndex(indexName));
        System.out.println(searchService.deleteIndex(indexName)?"删除成功":"删除失败");
        searchService.createIndex(indexName,typeName);
    }

    @Test
    public   void  test() throws  Exception{
        //数据
        List<TbItem> list = tbItemMapper.selectList(null);
        for (TbItem tbItem : list) {
            //把数据(db,mq)导入es
            String json = objectMapper.writeValueAsString(tbItem);
            searchService.addData(indexName,typeName,json,tbItem.getId()+"");
        }
    }

    @Test
    public   void  testUpdate() throws  Exception{
//        TbItem tbItem = new TbItem();
//        tbItem.setTitle("华为5G");
        Map map = new HashMap();
        map.put("title","华为5G");
        String json = objectMapper.writeValueAsString(map);

        System.out.println(json);
        //{"id":null,"title":"华为5G","sellPoint":null,"price":null,"num":null,"created":null,"updated":null}
        searchService.updateDoc(indexName,typeName,json,1182817+"");
        //1185017
    }

    @Test
    public  void  testDelete() throws  Exception{
        searchService.delete(indexName,typeName,"1185017");
        searchService.batchDelete(indexName,typeName,"1182817","1182913");
    }

    @Test
    public  void  queryAll() throws IOException {
        System.out.println(searchService.queryAll(indexName,typeName));
    }

    @Test
    public  void  matchQuery() throws IOException {
        System.out.println(searchService.matchQuery(indexName,typeName));
    }

    @Test
    public  void  multiMatchQuery() throws IOException {
        System.out.println(searchService.mulitmatchQuery(indexName,typeName));
    }

    @Test
    public  void testQuery() throws IOException {
        System.out.println(searchService.fuzzyQuery(indexName,typeName));
        System.out.println("0--------");
        System.out.println(searchService.rangeQuery(indexName,typeName));
    }

    @Test
    public  void testBoolQuery() throws IOException {
        System.out.println(searchService.boolQuery(indexName,typeName));
        System.out.println("0--------");
    }
    @Test
    public  void testAvgQuery() throws IOException {
        //System.out.println(searchService.avgQuery(indexName,typeName));
        System.out.println(searchService.MaxAndMinQuery(indexName,typeName));
    }


    @Test
    public  void testHighLighter() throws IOException {
        //System.out.println(searchService.avgQuery(indexName,typeName));
        System.out.println(searchService.matchQueryAndHightLight(indexName,typeName));
    }


}
