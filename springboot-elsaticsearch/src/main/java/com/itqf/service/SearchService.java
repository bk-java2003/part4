package com.itqf.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/15
 * @Time: 上午10:22
 */
public interface SearchService {

    //创建索引
    public   boolean   createIndex(String indexName,String type) throws IOException;
    //判断索引是否存在
    public  boolean  existsIndex(String indexName)  throws IOException;
    //删除索引
    public  boolean  deleteIndex(String indexName)  throws IOException;

    //添加数据  (有id则修改   无则新增)
    public   void   addData(String indexName,String type,String data,String id) throws IOException;

    public   void  updateDoc(String indexName,String type,String data,String id)throws IOException;

    public   void  delete(String indexName,String type,String id)throws IOException;

    public   void  batchDelete(String indexName,String type,String...ids)throws IOException;

    public List<Map> queryAll(String indexName, String type) throws  IOException;

    public List<Map> matchQuery(String indexName, String type) throws  IOException;

    public List<Map> mulitmatchQuery(String indexName, String type) throws  IOException;

    public List<Map> fuzzyQuery(String indexName, String type) throws  IOException;
    public List<Map> rangeQuery(String indexName, String type) throws  IOException;

    //复合查询
    public List<Map> boolQuery(String indexName, String type) throws  IOException;

    //求平均值
    public Map<String,Object> avgQuery(String indexName, String type) throws  IOException;

    //max  和 min
    public Map<String,Object> MaxAndMinQuery(String indexName, String type) throws  IOException;

    //匹配查询和高亮
    public List<Map> matchQueryAndHightLight(String indexName, String type) throws  IOException;

}
