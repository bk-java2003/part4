package com.itqf.entity;

import lombok.Data;

import java.util.Date;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/15
 * @Time: 上午9:23
 */
@Data
public class TbItem {
        private Long id;
         //`id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '商品id，同时也是商品编号',
        private  String title;//    `title` varchar(100) NOT NULL COMMENT '商品标题',
        private  String sellPoint; //  `sell_point` varchar(500) DEFAULT NULL COMMENT '商品卖点',
        private Long price;// bigint(20) NOT NULL COMMENT '商品价格，单位为：分',
        private Integer num;
        private Date  created;
        private Date  updated;



}
