package com.itqf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itqf.entity.TbItem;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/15
 * @Time: 上午9:22
 */
public interface TbItemMapper extends BaseMapper<TbItem> {
}
