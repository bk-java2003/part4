package com.itqf.utils;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/15
 * @Time: 上午9:28
 */
@Configuration  //ioc容器   结合@Bean注解使用
//@Component  //构建对象
public class EsClient {
    //EsClient  esclient = new EsClient();//
    //UsersDao usersDao  = new UsersDaoImpl();

    /**
     * 构建一个 操作es的客户端
     * @return
     */
     @Bean
    public   RestHighLevelClient client(){
        //1,HttpHost... hosts
        HttpHost httpHost = new HttpHost("192.168.82.168",9200);
        //2.RestClientBuilder
        RestClientBuilder clientBuilder = RestClient.builder(httpHost);
        //3.RestHighLevelClient
        RestHighLevelClient restHighLevelClient = new RestHighLevelClient(clientBuilder);

        return  restHighLevelClient;
    }



}
