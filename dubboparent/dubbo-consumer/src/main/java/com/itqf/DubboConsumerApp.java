package com.itqf;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/16
 * @Time: 下午4:30
 */
@SpringBootApplication
@EnableDubbo
public class DubboConsumerApp {
    public  static  void  main(String[]args){
        SpringApplication.run(DubboConsumerApp.class,args);
    }

}
