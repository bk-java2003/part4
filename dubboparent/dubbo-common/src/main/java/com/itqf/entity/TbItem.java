package com.itqf.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/16
 * @Time: 下午3:41
 */
@Data
@TableName("tb_item")
public class TbItem implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;
    private  String title;
    private  String sellPoint;
    private  long price;
    private  int num;
    private Date created;



}
