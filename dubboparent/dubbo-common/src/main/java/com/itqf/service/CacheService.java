package com.itqf.service;

/**
 * @Description:  缓存服务
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/16
 * @Time: 下午3:43
 */
public interface CacheService {

    public  void  set(String k,String v);

    public  String  get(String k);

    public  boolean  del(String k);


}
