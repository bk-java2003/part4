package com.itqf.service;

import com.itqf.entity.TbItem;

import java.util.List;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/16
 * @Time: 下午3:43
 */
public interface TbItemService {

    public List<TbItem> findAll();

    public int saveTbItem(TbItem tbItem);

}
