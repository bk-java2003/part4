package test;

import com.itqf.DubboProviderApp;
import com.itqf.mapper.TbItemMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/16
 * @Time: 下午4:00
 */
@RunWith(SpringJUnit4ClassRunner.class)  //org.junit.Test
@SpringBootTest(classes = DubboProviderApp.class)
@ContextConfiguration
public class TestSSMP {

    @Resource
    private TbItemMapper tbItemMapper;

    @Test
    public  void   test(){
        System.out.println(tbItemMapper.selectList(null));
    }

}
