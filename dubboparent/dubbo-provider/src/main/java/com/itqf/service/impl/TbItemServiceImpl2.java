package com.itqf.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itqf.entity.TbItem;
import com.itqf.mapper.TbItemMapper;
import com.itqf.service.TbItemService;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/16
 * @Time: 下午3:48
 */
@Service(version = "3.0")  //com.alibaba.dubbo.config.annotation.Service
public class TbItemServiceImpl2 implements TbItemService {

    @Resource
    private TbItemMapper tbItemMapper;

    @Override
    public List<TbItem> findAll() {
        return tbItemMapper.selectList(null);
    }

    @Override
    public int saveTbItem(TbItem tbItem) {
        return tbItemMapper.insert(tbItem);
    }
}
