package com.itqf.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itqf.service.CacheService;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/16
 * @Time: 下午3:48
 */
@Service(version = "4.0")
public class CacheServiceImpl implements CacheService {

    @Resource
    RedisTemplate<String,String> redisTemplate;
    //RabbitMQTemplate   RestTemplate  JdbcTemplate


    @Override
    public void set(String k, String v) {
        redisTemplate.opsForValue().set(k,v);
    }

    @Override
    public String get(String k) {
        return redisTemplate.opsForValue().get(k);
    }

    @Override
    public boolean del(String k) {
        return redisTemplate.delete(k);
    }
}
