package com.itqf;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/16
 * @Time: 下午4:02
 */
@SpringBootApplication
@MapperScan(basePackages = "com.itqf.mapper")
@EnableDubbo   //开启dubbo
//springcloud
public class DubboProviderApp {

    public  static  void  main(String[]args){
        SpringApplication.run(DubboProviderApp.class,args);
    }

}
