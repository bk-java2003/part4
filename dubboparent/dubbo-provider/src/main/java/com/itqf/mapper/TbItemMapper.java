package com.itqf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itqf.entity.TbItem;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/16
 * @Time: 下午3:59
 */
public interface TbItemMapper extends BaseMapper<TbItem> {
}
