package com.itqf.demo01;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.cache.NodeCache;
import org.apache.curator.framework.recipes.cache.NodeCacheListener;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/16
 * @Time: 上午10:44
 */
public class TestZk01 {
    public  static  void  main(String[]args) throws  Exception{
        //createNode();
        //getData();
        //得到节点下的数据
        //deleteNode();

        RetryPolicy retryPolicy = new ExponentialBackoffRetry(6000,2);
        CuratorFramework cf = CuratorFrameworkFactory.builder()
                .connectString("192.168.82.168:2181,192.168.82.168:2182,192.168.82.168:2183")
                .sessionTimeoutMs(9000)
                .retryPolicy(retryPolicy)
                .build();
        cf.start();
        //建立一个cache缓存,第三个参数为是否压缩
        NodeCache nodeCache = new NodeCache(cf,"/cfnode",false);
        nodeCache.start(true);
        //开始监听
        nodeCache.getListenable().addListener(new NodeCacheListener() {
            @Override
            public void nodeChanged() throws Exception {
                System.out.println("路径为：" + nodeCache.getCurrentData().getPath());
                System.out.println("数据为：" + new String(nodeCache.getCurrentData().getData()));
                System.out.println("状态为：" + nodeCache.getCurrentData().getStat());
            }
        });


        cf.create().forPath("/cfnode","thanks".getBytes());
        Thread.sleep(3000);
        cf.setData().forPath("/cfnode","beijing".getBytes());
        Thread.sleep(3000);
        cf.delete().forPath("/cfnode");//删除节点
//        Thread.sleep(3000);
        cf.close();



    }

    private static void deleteNode() throws Exception {
        //1,创建重试策略
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(6000,3);
        //2.创建连接客户端
        CuratorFramework cf = CuratorFrameworkFactory.builder()
                //连接的地址   集群 使用逗号隔开
                .connectString("192.168.82.168:2181,192.168.82.168:2182,192.168.82.168:2183") //ip:port  ,192.168.82.168:2182,192.168.82.168:2183
                .retryPolicy(retryPolicy)
                .sessionTimeoutMs(9000)
                .build();

        //3.开启连接
        cf.start();

        //删除多级目录
        cf.delete().deletingChildrenIfNeeded().forPath("/qf2");
        cf.delete().forPath("/qf1");//单级目录

        cf.close();
    }

    private static void getData() throws Exception {
        //得到节点下的数据
        //1,创建重试策略
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(6000,3);
        //2.创建连接客户端
        CuratorFramework cf = CuratorFrameworkFactory.builder()
                //连接的地址   集群 使用逗号隔开
                .connectString("192.168.82.168:2181,192.168.82.168:2182,192.168.82.168:2183") //ip:port  ,192.168.82.168:2182,192.168.82.168:2183
                .retryPolicy(retryPolicy)
                .sessionTimeoutMs(9000)
                .build();

        //3.开启连接
        cf.start();

        String s = new String(cf.getData().forPath("/qf1"));

        System.out.println(s);

        cf.close();
    }

    private static void createNode() throws Exception {
        //1,创建重试策略
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(6000,3);
        //2.创建连接客户端
        CuratorFramework cf = CuratorFrameworkFactory.builder()
                //连接的地址   集群 使用逗号隔开
                .connectString("192.168.82.168:2181,192.168.82.168:2182,192.168.82.168:2183") //ip:port  ,192.168.82.168:2182,192.168.82.168:2183
                .retryPolicy(retryPolicy)
                .sessionTimeoutMs(9000)
                .build();

        //3.开启连接
        cf.start();

        System.out.println(cf);
        //4.创建节点
        String s=  cf.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT_SEQUENTIAL).forPath("/qf2/java","bkjava".getBytes());
        //5.临时节点
        cf.create().withMode(CreateMode.EPHEMERAL).forPath("/jabva");
        cf.create().withMode(CreateMode.EPHEMERAL_SEQUENTIAL).forPath("/h4");
        System.out.println(s);

        Thread.sleep(10000);


        cf.close();//临时节点只有客户端关闭，就自动删除
    }
}
