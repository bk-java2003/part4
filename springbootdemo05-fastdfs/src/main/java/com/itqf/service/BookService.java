package com.itqf.service;

import com.itqf.entity.Book;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/11
 * @Time: 上午11:24
 */
public interface BookService {
    public   int  addBook(Book book);
}
