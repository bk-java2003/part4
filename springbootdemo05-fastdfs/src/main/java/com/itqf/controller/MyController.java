package com.itqf.controller;

import com.github.tobato.fastdfs.domain.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import com.itqf.entity.Book;
import com.itqf.service.BookService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/11
 * @Time: 上午9:43
 */
@RestController
public class MyController {

    @Resource //fastdfs传图片的客户端对象
    private FastFileStorageClient fastFileStorageClient;

    @Resource
    private BookService bookService;


    @Value("${fdfs.web-server-url}")
    private  String serverUrl;

    @RequestMapping("/uploadFile")
    public Map upload(MultipartFile file){

        Map map = new HashMap();
        try {
            String  name = file.getOriginalFilename();
            //jdiwojdiwfie.jpg
            String suffix  = name.substring(name.lastIndexOf(".")+1);
            //把图片存储到图片服务器中   nginx
            //(InputStream inputStream, long fileSize, String fileExtName, Set<MetaData> metaDataSet
            //传图片
            StorePath path =  fastFileStorageClient.uploadFile(file.getInputStream(),file.getSize(),suffix,null);
            String  filepath = path.getFullPath();
            //group1/00/00/dhiwhdiwfhiwfh.jpg
            //http://192.168.82.168/group1/00/00/dhiwhdiwfhiwfh.jpg
            map.put("code",0);
            map.put("msg","上传成功");
            map.put("path",serverUrl+filepath);

            return  map;

        } catch (IOException e) {
            e.printStackTrace();
        }
        map.put("code",1);
        map.put("msg","上传失败");

        return  map;

    }

    @RequestMapping("/addBook")
    public  Map addBook(Book book){//INSERT INTO book    VALUES
        Map map = new HashMap();
       int i= bookService.addBook(book);
        if (i>0){
            map.put("code",0);
            map.put("msg","新增成功");

        }else{
        map.put("code",1);
        map.put("msg","新增失败");}
        return  map;

    }




}
