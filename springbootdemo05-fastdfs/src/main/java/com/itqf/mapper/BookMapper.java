package com.itqf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itqf.entity.Book;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/11
 * @Time: 上午11:24
 */
public interface BookMapper extends BaseMapper<Book> {
}
