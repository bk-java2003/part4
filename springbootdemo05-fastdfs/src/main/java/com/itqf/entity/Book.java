package com.itqf.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/11
 * @Time: 上午11:20
 */
@Data
@TableName("book")
public class Book {

    @TableId(type = IdType.AUTO)
    //@Id
    private  Integer id;
    private String name;
    private String author;
    private String images;
    private  Integer tId;


}
