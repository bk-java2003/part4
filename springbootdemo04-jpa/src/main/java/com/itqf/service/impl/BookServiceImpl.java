package com.itqf.service.impl;

import com.itqf.dao.BookDao;
import com.itqf.entity.Book;
import com.itqf.service.BookService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/9
 * @Time: 下午4:30
 */
@Service
public class BookServiceImpl implements BookService {

    @Resource
    private BookDao bookDao;

    @Override
    public List<Book> findAll() {
        return bookDao.findAll();
    }

    @Override
    public void save(Book book) {

        bookDao.save(book);
    }

    @Override
    public void update(Book book) {
        bookDao.saveAndFlush(book);//保存和更新
    }

    @Override
    public void delete(int id) {
        bookDao.deleteById(id);
    }

    @Override
    public Book findById(int id) {
        Optional<Book> byId =  bookDao.findById(id);
        return byId.get();
    }
}
