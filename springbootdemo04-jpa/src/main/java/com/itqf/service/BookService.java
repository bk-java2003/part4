package com.itqf.service;

import com.itqf.entity.Book;

import java.util.List;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/9
 * @Time: 下午4:29
 */
public interface BookService {

    public List<Book> findAll();

    public  void  save(Book book);

    public  void  update(Book book);

    public  void  delete(int id);
    public  Book  findById(int id);



}
