package com.itqf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springbootdemo04JpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springbootdemo04JpaApplication.class, args);
    }

}
