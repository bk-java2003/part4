package com.itqf.dao;

import com.itqf.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/9
 * @Time: 下午4:27
 */
//<T, ID>
 //public interface BookDao  extends BaseMapper<Book> {
//}
@Repository   //dao  构建对象
public interface BookDao  extends JpaRepository<Book,Integer> {
}
