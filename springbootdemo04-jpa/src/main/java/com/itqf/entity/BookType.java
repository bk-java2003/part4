package com.itqf.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/9
 * @Time: 下午5:19
 */
@Table(name = "book_type")
@Entity
@Data
public class BookType {

    @Id//映射注解  mybatis-plu  @TableId
    @GeneratedValue(strategy=GenerationType.AUTO)
    private  int id;

    @Column(name = "name")
    private  String name;


    //private Book book;
    //it  一对多
    /**
     * cascade = CascadeType.ALL   级联
     * fetch = FetchType.EAGER  是否立即加载
     mappedBy="t_id"  外键字段
     */
    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER
    ,mappedBy="t_id")
    private List<Book> bookList;



}
