package com.itqf.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/9
 * @Time: 下午4:24
 */
@Table(name = "book")//数据库的表名
@Entity
@Data
public class Book {

    @Id//映射注解  mybatis-plu  @TableId
    @GeneratedValue(strategy=GenerationType.AUTO)
    private  int id;

    @Column(name = "name")
    private  String name;
    @Column(name = "author")
    private  String author;

    @Column(name = "t_id")
    private  int t_id;//类型编号




}
