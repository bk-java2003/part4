package com.itqf;

import com.itqf.dao.BookTypeDao;
import com.itqf.entity.Book;
import com.itqf.service.BookService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;

@SpringBootTest(classes = Springbootdemo04JpaApplication.class)
@ContextConfiguration
class Springbootdemo04JpaApplicationTests {

    @Resource
    private BookService bookService;

    @Resource
    private BookTypeDao bookTypeDao;

    @Test
    void contextLoads() {


        Book book = new Book();
        book.setAuthor("亚洲舞王.赵四");
        book.setName("JVM");
        book.setT_id(1);
        //query
        bookService.save(book);

        book.setId(1);

        //bookService.update(book);

        //bookService.delete(1);

        System.out.println(bookService.findAll());


    }

    @Test
    public  void  testOneToMany(){


        //System.out.println(bookTypeDao.findAll());
        bookTypeDao.deleteById(1);//级联删除  先删除字表数据   再删除主表数据

        /**
         * 最后效果
         * Hibernate: delete from book where id=?
         * Hibernate: delete from book where id=?
         * Hibernate: delete from book_type where id=?
         */


    }

}
