package com.itqf;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;

@SpringBootTest(classes = SpringbootMqConsumerApplication.class)
@ContextConfiguration
class SpringbootMqConsumerApplicationTests {

    @Resource
    //starter-data-redis
    //RabbitMqTemplate  AmqpTemplate  JdbcTemplate
    //RestTemplate
    private RedisTemplate redisTemplate;


    @Test
    void contextLoads() {

        //String
        redisTemplate.opsForValue().set("name","张三");
        System.out.println(redisTemplate.opsForValue().get("name"));

        redisTemplate.opsForHash().put("user","uname","李四");
        System.out.println(redisTemplate.opsForHash().get("user","uname"));




    }

}
