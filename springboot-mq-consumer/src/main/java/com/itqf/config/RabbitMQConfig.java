package com.itqf.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/12
 * @Time: 下午2:51
 */
@Configuration
public class RabbitMQConfig {

    @Bean
    public Queue queue(){
        return new Queue("springboot-queue");
    }

//    @Bean
//    public FanoutExchange fanoutExchange(){
//
//        return  new FanoutExchange("springboot-fanout-exchange",false,false);
//    }

//    @Bean
//    public Binding binding(Queue queue,FanoutExchange fanoutExchange){
//        //绑定队列和交换机
//        return BindingBuilder.bind(queue).to(fanoutExchange);
//    }

    @Bean
    public DirectExchange directExchange(){
        return new DirectExchange("springboot-direct-exchange",false,false);
    }

//    @Bean
//    public TopicExchange topicExchange(){
//        return new TopicExchange("springboot-topic-exchange",false,false);
//    }

    //queue  exchange  channel.bindQueue(queue,echange)
    //routingKey
   @Bean
    public  Binding binding(Queue queue,DirectExchange directExchange){
        //info  error
        return  BindingBuilder.bind(queue).to(directExchange).with("error");
        //topic  with("#.#");//aaa
   }



}
