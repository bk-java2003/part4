package com.itqf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.sidecar.EnableSidecar;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/19
 * @Time: 上午9:20
 */
@SpringBootApplication
@EnableEurekaClient
@EnableSidecar //开启sideCar的代理访问
//代理访问第三方(非springcloud的项目   可能ssm，python....)
public class SidecarApplication {
    public  static  void  main(String[]args){
        SpringApplication.run(SidecarApplication.class,args);
    }
}
