package test;

import com.itqf.StreamSendApplication;
import com.itqf.mq.StreamClientSend;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/19
 * @Time: 上午10:41
 */
@SpringBootTest(classes = StreamSendApplication.class)
@ContextConfiguration
public class TestStreamSend {

    @Resource
    StreamClientSend streamClientSend;

    @Test
    public  void  test(){
        //测试发送消息
        streamClientSend.output().send(MessageBuilder.withPayload("hello，我是springcloud的stream组件发送的消息").build());

    }

}
