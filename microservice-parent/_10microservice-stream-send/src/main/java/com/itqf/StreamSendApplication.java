package com.itqf;

import com.itqf.mq.StreamClientSend;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.stream.annotation.EnableBinding;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/19
 * @Time: 上午10:39
 */
@SpringBootApplication
@EnableEurekaClient
@EnableBinding(StreamClientSend.class)//开启stream组件
public class StreamSendApplication {
    public  static  void  main(String[]args){

        SpringApplication.run(StreamSendApplication.class,args);

    }
}
