package com.itqf.mq;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/19
 * @Time: 上午10:38
 */

public interface StreamClientSend {

    @Output("MyStreamMessage1")
    public MessageChannel output();

}
