package com.itqf.controller;

import com.itqf.service.TbItemService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/18
 * @Time: 上午9:40
 */
@RestController
public class MyController {

    @Resource
    private TbItemService tbItemService;

    @RequestMapping("/find")
    public Map find(){
        return  tbItemService.find();
    }


    @RequestMapping("/find1")
    public Map find1(){
        return  tbItemService.find1();
    }

    @RequestMapping("/findById1/{id}")
    public Map find1(@PathVariable int id){
        return  tbItemService.findById1(id);
    }
    @RequestMapping("/findById/{id}")
    public Map findById(@PathVariable int id){
        return  tbItemService.findById(id);
    }

    @RequestMapping("/findByCache/{id}")
    public Map findByCache(@PathVariable int id){

        tbItemService.findByIdCache(1);
        tbItemService.findByIdCache(1);//走缓存
        //
        System.out.println("清除缓存");
        tbItemService.clearCache(1);//清缓存
        tbItemService.findByIdCache(1);

        System.out.println("参数变了");
        tbItemService.findByIdCache(2);//重新请求该方法
        tbItemService.findByIdCache(2);//缓存


        return  tbItemService.findById(id);
    }


}
