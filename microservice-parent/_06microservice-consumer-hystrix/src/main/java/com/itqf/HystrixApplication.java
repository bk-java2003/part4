package com.itqf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/17
 * @Time: 下午3:57
 */
@SpringBootApplication

@ServletComponentScan(basePackages = "com.itqf.servlet")//扫描servlet的注解@WebServlet("/hystrix.stream")
@EnableEurekaClient
@EnableFeignClients //开启openfeign的调用
//@EnableHystrix  //开启服务降级 .....
@EnableCircuitBreaker
@EnableHystrixDashboard //开启熔断 监控
//  localhost:8086/hystrix
public class HystrixApplication {


    public  static  void  main(String[]args){

        SpringApplication.run(HystrixApplication.class,args);
    }
}
