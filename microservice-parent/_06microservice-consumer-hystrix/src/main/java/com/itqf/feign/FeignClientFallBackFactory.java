package com.itqf.feign;

import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/18
 * @Time: 上午9:12
 */
@Component
public class FeignClientFallBackFactory implements FallbackFactory<FeignClientService> {

    @Resource
    private  FeignClientServiceFallback feignClientServiceFallback;

    @Override
    public FeignClientService create(Throwable throwable) {
        System.out.println("报错信息："+throwable.getLocalizedMessage());
        throwable.printStackTrace();
        return feignClientServiceFallback;
    }
}
