package com.itqf.feign;

import com.itqf.entity.TbItem;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/17
 * @Time: 下午3:59
 */
//提供者 spring.application.name=
    //fallback 调用提供者时，提供者出问题（异常，超时），走fallback.
    //fallback = FeignClientServiceFallback.class
@FeignClient(serviceId = "eureka-client-provider")
public interface FeignClientService {

    @RequestMapping("/find") //提供者提供的接口的url完全一致
    public Map find();//伪装调用提供者的/find接口

    @RequestMapping("/findById/{id}")
    public  Map  findById(@PathVariable("id") int id);

    @RequestMapping("/save")
    //RequestParam.value() was empty on parameter 0
    public  Map  save(@RequestParam("id") int id, @RequestParam("name") String name);

    @RequestMapping("/saveJson")
    public  TbItem  saveJson(@RequestBody TbItem tbItem);


}
