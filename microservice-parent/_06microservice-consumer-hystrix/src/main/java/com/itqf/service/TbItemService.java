package com.itqf.service;

import java.util.Map;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/18
 * @Time: 上午9:35
 */
public interface TbItemService {

    public Map find();

    public Map findById(int id);
    public Map findById1(int id) ;

    public Map find1() ;

    public Map findByIdCache(int id) ;
    public Map clearCache(int id) ;


    }
