package com.itqf.service.impl;

import com.itqf.feign.FeignClientService;
import com.itqf.service.TbItemService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.netflix.hystrix.contrib.javanica.cache.annotation.CacheKey;
import com.netflix.hystrix.contrib.javanica.cache.annotation.CacheRemove;
import com.netflix.hystrix.contrib.javanica.cache.annotation.CacheResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/18
 * @Time: 上午9:35
 */
@Service
public class TbItemServiceImpl implements TbItemService {

    @Resource
    private FeignClientService feignClientService;

    //execution.isolation.strategy=THREAD（自己的线程池）,SEMAPHORE(信号量,走tomcat的线程，使用信号量控制线程);
    //加了如下设置（name="execution.isolation.strategy",value = "THREAD") 走hystrix自己的线程池
    @HystrixCommand(fallbackMethod = "findFallback",
    commandProperties ={@HystrixProperty(name="execution.isolation.strategy",value = "THREAD"),
       @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "2000"),
    } )
    @Override
    public Map find() {
        System.out.println(Thread.currentThread().getName()+"---->find");
        //HystrixCommandProperties  定义了各种属性设置   线程隔离  熔断器
        return feignClientService.find();
    }

    //execution.isolation.strategy=THREAD（自己的线程池）,SEMAPHORE(信号量,走tomcat的线程，使用信号量控制线程);
    //加了如下设置（name="execution.isolation.strategy",value = "THREAD") 走hystrix自己的线程池
    @HystrixCommand(fallbackMethod = "findFallback",
            commandProperties ={@HystrixProperty(name="execution.isolation.strategy",value = "THREAD"),
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "2000"),
                    @HystrixProperty(name = "circuitBreaker.enabled",value = "true"),//打开
                    @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold",value = "10"),
                    @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage",value = "50"),
                    @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds",value = "5000")
    })
    @Override
    public Map findById(int id) {
        try {
            long time = new Random().nextInt(3000);
            System.out.println(time);
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"---->find");
        return feignClientService.findById(1);
    }

    //SEMAPHORE  信号量  tomcat 线程池
    @HystrixCommand(fallbackMethod = "findFallback",
            commandProperties ={@HystrixProperty(name="execution.isolation.strategy",value = "SEMAPHORE"),
                    @HystrixProperty(name = "execution.isolation.semaphore.maxConcurrentRequests",value = "10"),
            } )
    @Override
    public Map findById1(int id) {

        System.out.println(Thread.currentThread().getName()+"---->find");
        return feignClientService.findById(1);
    }

    public Map findFallback(int id) {

        Map map = new HashMap();
        map.put("code",-1);
        map.put("msg","服务器丢了，请稍后再试  findById");

        return  map;
    }

    //
    public Map findFallback() {

        Map map = new HashMap();
        map.put("code",-1);
        map.put("msg","服务器丢了，请稍后再试");

        return  map;
    }

    /**
     * 走默认的（tomcat的线程池）
     * @return
     */
    public Map find1() {
        System.out.println("哈哈哈");
        System.out.println(Thread.currentThread().getName()+"---->find1");
        Map map = new HashMap();
        map.put("code",1);
        map.put("msg","测试hystrix的线程隔离");

        return map;
    }


    @CacheResult
    @HystrixCommand(commandKey = "findById")
    @Override
    public Map findByIdCache(@CacheKey int id) {
        System.out.println("测试缓存！！！");
        System.out.println(Thread.currentThread().getName()+"---->find1");
        Map map = new HashMap();
        map.put("code",1);
        map.put("msg","测试hystrix的线程隔离");

        return map;
    }

    @CacheRemove(commandKey = "findById")
    @Override
    public Map clearCache(@CacheKey int id) {
        return null;
    }
}
