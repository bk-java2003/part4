package com.itqf.servlet;

import com.netflix.hystrix.strategy.concurrency.HystrixRequestContext;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/18
 * @Time: 下午3:04
 */
@WebFilter("/*")
public class HYstrixInitFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        //Maybe you need to initialize the HystrixRequestContext?
        HystrixRequestContext.initializeContext();//初始化
        filterChain.doFilter(servletRequest,servletResponse);


    }
}
