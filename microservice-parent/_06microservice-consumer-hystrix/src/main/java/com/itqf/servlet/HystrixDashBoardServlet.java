package com.itqf.servlet;

import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;

import javax.servlet.annotation.WebServlet;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/18
 * @Time: 上午11:18
 */
@WebServlet("/hystrix.stream")
public class HystrixDashBoardServlet extends HystrixMetricsStreamServlet {
}
