package com.itqf.test;

import com.itqf.FeignApplication;
import com.itqf.entity.TbItem;
import com.itqf.feign.FeignClientService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/17
 * @Time: 下午4:12
 */
@SpringBootTest(classes = FeignApplication.class)
@ContextConfiguration
public class TestFeign {

    @Resource
    private FeignClientService feignClientService;

    @Test
    public  void  test(){


        System.out.println(feignClientService.find());

        System.out.println(feignClientService.findById(2333));
        System.out.println(feignClientService.save(1,"dnisdnw "));

        TbItem tbItem = new TbItem();
        tbItem.setId(222);
        tbItem.setName("张三");
        System.out.println(feignClientService.saveJson(tbItem));

    }


}
