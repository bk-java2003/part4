package com.itqf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/17
 * @Time: 下午3:57
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients //开启openfeign的调用
public class FeignApplication {

    public  static  void  main(String[]args){
        SpringApplication.run(FeignApplication.class,args);
    }
}
