package com.itqf.feign;

import com.itqf.entity.TbItem;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/17
 * @Time: 下午5:26
 */
@Component
public class FeignClientServiceFallback implements  FeignClientService {
    @Override
    public Map find() {
        System.out.println("---->服务器忙！！");
        Map map = new HashMap();
        map.put("code",-1);
        map.put("msg","服务器忙");
        return map;
    }

    @Override
    public Map findById(int id) {
        return null;
    }

    @Override
    public Map save(int id, String name) {
        return null;
    }

    @Override
    public TbItem saveJson(TbItem tbItem) {
        return null;
    }
}
