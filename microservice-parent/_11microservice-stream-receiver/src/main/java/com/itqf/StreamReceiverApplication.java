package com.itqf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/19
 * @Time: 上午10:39
 */
@SpringBootApplication
@EnableEurekaClient
public class StreamReceiverApplication {
    public  static  void  main(String[]args){

        SpringApplication.run(StreamReceiverApplication.class,args);

    }
}
