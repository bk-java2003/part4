package com.itqf.mq;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/19
 * @Time: 上午10:38
 */

public interface StreamClientReceiver {

    @Input("MyStreamMessage1")
    public SubscribableChannel input();

}
