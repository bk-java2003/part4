package com.itqf.listener;

import com.itqf.mq.StreamClientReceiver;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/19
 * @Time: 上午10:48
 */
@Component
@EnableBinding({StreamClientReceiver.class})
public class MessageListener {

    @StreamListener("MyStreamMessage1")
    public  void  onMessage(String msg, @Header(AmqpHeaders.CHANNEL) Channel channel,
                            @Header(AmqpHeaders.DELIVERY_TAG) long  deliveryTag){

        try {
            System.out.println("消息是"+msg);
                int i  = 10/0;
            channel.basicAck(deliveryTag,false);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
