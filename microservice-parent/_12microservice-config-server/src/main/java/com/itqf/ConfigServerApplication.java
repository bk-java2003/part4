package com.itqf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/19
 * @Time: 上午11:55
 */
@SpringBootApplication
@EnableEurekaClient
@EnableConfigServer  //开启config组件
public class ConfigServerApplication {
    public  static  void  main(String[]args){
        SpringApplication.run(ConfigServerApplication.class,args);
    }
}
