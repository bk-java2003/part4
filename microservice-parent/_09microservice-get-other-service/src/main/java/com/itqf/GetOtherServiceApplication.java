package com.itqf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/19
 * @Time: 上午9:29
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class GetOtherServiceApplication {
    public  static  void  main(String[]args){
        SpringApplication.run(GetOtherServiceApplication.class,args);
    }
}
