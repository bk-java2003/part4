package com.itqf.client;

import com.itqf.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/19
 * @Time: 上午9:28
 */
//通过 sidecar项目 调用第三方的服务
@FeignClient(serviceId = "microservice-sidecar")
public interface SidecarFeignClient {

    @RequestMapping("/finance/doctorDuibi")
    public R  findDoctor(@RequestParam("page")int page,@RequestParam("limit")int limit);

}
