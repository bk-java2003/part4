package com.test;

import com.itqf.GetOtherServiceApplication;
import com.itqf.client.SidecarFeignClient;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/19
 * @Time: 上午9:34
 */
@SpringBootTest(classes = GetOtherServiceApplication.class)
@ContextConfiguration
public class TestSidecarClient {

    @Resource
    private SidecarFeignClient sidecarFeignClient;

    @Test
    public  void  test(){

        System.out.println(sidecarFeignClient.findDoctor(1,3));


    }


}
