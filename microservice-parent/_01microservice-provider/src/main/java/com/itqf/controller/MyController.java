package com.itqf.controller;

import com.itqf.entity.TbItem;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/17
 * @Time: 上午9:43
 */
@Controller
public class MyController {

    @Value("${version}")
    private  String version;


    @RequestMapping("/find")
    @ResponseBody
    public Map<String,Object> find(){
        Map map = new HashMap();
        map.put("id",1);
        map.put("name","springcloud");

        int  i= 10/0;

        System.out.println("---->8082");
        return  map;
    }

    @RequestMapping("/findById/{id}")
    @ResponseBody
    public Map<String,Object> find(@PathVariable int id){
        Map map = new HashMap();
        map.put("id",id);
        map.put("name","springcloud");
        map.put("desc","就是牛！！！！！！");

        System.out.println("版本："+version);

        return  map;
    }

    @RequestMapping("/save")
    @ResponseBody
    public Map<String,Object> save(@RequestParam int id,@RequestParam String  name){
        Map map = new HashMap();
        map.put("id",id);
        map.put("name",name);
        map.put("desc","就是牛！！！！！！");

        return  map;
    }
    @RequestMapping("/saveJson")
    @ResponseBody
    public TbItem  save(@RequestBody TbItem tbItem){


        return  tbItem;
    }


}
