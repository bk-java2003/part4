package com.itqf;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/17
 * @Time: 上午11:44
 */
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
       //eureka/**：首斜杠表示应用上下文路径，也就是说对所有符合 http://ip:port/context-paht/eureka/** 的请求全部忽略对它们的 CSRF 防护
        // * 所以只要 eureka.client.serviceUrl.defaultZone 的值符合此规则，就不会被 CSRF 防护了
        // csrf()  csrf校验  ignoringAntMatchers 忽略路径
        http.csrf().ignoringAntMatchers("/eureka/**");
        super.configure(http);
    }
}

