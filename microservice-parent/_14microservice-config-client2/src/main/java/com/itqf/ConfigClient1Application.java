package com.itqf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/19
 * @Time: 下午2:43
 */
@SpringBootApplication
@EnableEurekaClient
public class ConfigClient1Application {
    public  static  void  main(String[]args){
        SpringApplication.run(ConfigClient1Application.class,args);
    }
}
