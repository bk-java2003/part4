package com.itqf.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/19
 * @Time: 下午2:44
 */
@RestController
@RefreshScope
//配置后执行该请求可以自动拉取最新配置

//curl -X POST http://localht:8093/actuator/refresh
public class MyController {

    @Value("${config.name}")
    private  String configName;

    @Value("${configtest}")
    private  String   test;

    @RequestMapping("/testConfig")
    public   String   testConfig(){
        System.out.println(configName+test);
        return "test:"+"--"+test;
    }

}
