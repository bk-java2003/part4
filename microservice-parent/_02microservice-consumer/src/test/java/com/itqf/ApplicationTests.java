package com.itqf;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Map;

@SpringBootTest(classes = ConsumerApplication.class)
@ContextConfiguration
class ApplicationTests {

    @Resource
    private RestTemplate restTemplate;
    /**
     *  No qualifying bean of type 'org.springframework.web.client.RestTemplate' available: expected at least 1 bean which qualifies as autowire candidate
     */

    @Resource
    private EurekaClient eurekaClient;


    @Test
    void contextLoads() {
        //dubbo RPC
        //restTemplate  http

        //System.out.println(restTemplate.getForEntity("http://localhost:8080/find",Map.class));
        //强耦合   192.172.32.34

        //参数一：应用名spring.application.name=microservice-provider
        InstanceInfo instanceInfo = eurekaClient.getNextServerFromEureka("EUREKA-CLIENT-PROVIDER",false);

        String url = instanceInfo.getHomePageUrl();

       Map map =  restTemplate.getForObject(url+"/find",Map.class);

        System.out.println(map);

    }

}
