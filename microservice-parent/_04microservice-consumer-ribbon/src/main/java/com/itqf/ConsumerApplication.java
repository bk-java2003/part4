package com.itqf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
public class ConsumerApplication {

    @Bean
    @LoadBalanced //开启负载均衡
    @ConditionalOnMissingClass //有则忽略  无则构建对象并放到IoC容器
    public RestTemplate restTemplate(){
        return  new RestTemplate();
    }



//    @Bean
//    public IRule iRule(){
//        System.out.println("----->RandomRule......");
//        return  new RandomRule();
//    }


    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class, args);
    }

}
