package com.itqf.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/17
 * @Time: 下午2:56
 */
@RestController
public class MyController {

    @Resource
    private RestTemplate restTemplate;

    @RequestMapping("/testRibbon")
    public Map find(){

//       InstanceInfo instanceInfo= eurekaClient.getNextServerFromEureka("EUREKA-CLIENT-PROVIDER",false);
//       String url =instanceInfo.getHomePageUrl();
       //System.out.println("----->"+url);
        // 返回的地址和真正调用的服务不一致
        //想看真正调用的服务，在服务端分别打印8080 和 8084

       Map map =  restTemplate.getForObject("http://EUREKA-CLIENT-PROVIDER/find",Map.class);

       return  map;
    }


}
