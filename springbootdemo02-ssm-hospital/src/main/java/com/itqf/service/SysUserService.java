package com.itqf.service;

import com.itqf.entity.SysUsers;
import com.itqf.utils.R;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/1
 * @Time: 上午11:15
 */
public interface SysUserService {
    public SysUsers findByLoginname(String loginname);

    public R findAllUser(int page, int limit, SysUsers sysUsers);

    public   R  updateUser(SysUsers sysUsers);

    public  SysUsers findById(int userid);

    public  R deleteByUser(int userid);

    /**
     * 分配角色
     * @param userid
     * @param ids
     * @return
     */
    public   R  saveUserRole(int userid, Integer[] ids);

    /**
     * 新增用户
     * @param sysUsers
     * @return
     */
    public   R  saveUser(SysUsers sysUsers);


}
