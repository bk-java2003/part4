package com.itqf.service;

import com.itqf.utils.R;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/4
 * @Time: 上午10:03
 */
public interface PharmacyService {
    public R findDrug(int page, int limit);
}
