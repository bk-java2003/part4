package com.itqf.service;

import com.itqf.entity.Departments;
import com.itqf.utils.R;

import java.util.List;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/3
 * @Time: 上午10:06
 */
public interface DepartmentsService {

    public List<Departments>  findAllDepartments();


    public R findByPage(int page, int limit, Departments departments);


}
