package com.itqf.service;

import com.itqf.dto.RegisterInfoDTO;
import com.itqf.entity.Register;
import com.itqf.utils.R;

import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/3
 * @Time: 上午11:08
 */
public interface RegisterService {

    public  int check(String phone, String carid);

    public R  saveRegister(Register register);

    public List<RegisterInfoDTO>  findAll(Integer cc, String name);

    public R  findByPage(int page, int limit);

    public  String  findReason(int regId);

    public  R  addReason(int regId, String bing);

    //public List<RegisterInfoDTO>  findAll(int page,int limit);

    public  List<Double>  findLineData(int year);

    public List<Map<String,Object>> findYearPieData();


    public List<Map<String,Object>> findCurrentPieData(String current);

    public R findDoctorData(int page, int limit);


}
