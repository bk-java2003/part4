package com.itqf.service.impl;

import com.alibaba.druid.util.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itqf.entity.SysUsers;
import com.itqf.mapper.SysUserMapper;
import com.itqf.service.SysUserService;
import com.itqf.utils.R;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/1
 * @Time: 上午11:16
 */
@Service
public class SysUserServiceImpl implements SysUserService {

    @Resource
    private SysUserMapper sysUserMapper;

    @Override
    public SysUsers findByLoginname(String loginname) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("loginname",loginname);

        return sysUserMapper.selectOne(queryWrapper);

    }


    @Override
    public R findAllUser(int page, int limit,SysUsers sysUsers) {

        IPage iPage = new Page(page,limit);
        if (sysUsers!=null){
            QueryWrapper queryWrapper = new QueryWrapper();
            if (!StringUtils.isEmpty(sysUsers.getRealname())){//null ""
                queryWrapper.like("realname",sysUsers.getRealname());
            }
            if (!StringUtils.isEmpty(sysUsers.getLoginname())){//null ""
                queryWrapper.like("loginname",sysUsers.getLoginname());
            }
            if (!StringUtils.isEmpty(sysUsers.getPosition())){//null ""
                queryWrapper.like("position",sysUsers.getPosition());
            }
            if (!StringUtils.isEmpty(sysUsers.getPhone())){//null ""
                queryWrapper.like("phone",sysUsers.getPhone());
            }
            if (!StringUtils.isEmpty(sysUsers.getIdentity())){//null ""
                queryWrapper.like("identity",sysUsers.getIdentity());
            }
            iPage = sysUserMapper.selectPage(iPage,queryWrapper);

        }else{
            iPage = sysUserMapper.selectPage(iPage,null);

        }

        List<SysUsers> list = iPage.getRecords();
        long total  = iPage.getTotal();

        //{code:0,data:[{}],msg:""}
        return R.ok("").put("data",list).put("count",total);
    }

    @Override
    public R updateUser(SysUsers sysUsers) {

        return sysUserMapper.updateById(sysUsers)>0?R.ok():R.fail("修改失败");
    }

    @Override
    public SysUsers findById(int userid) {
        return sysUserMapper.selectById(userid);
    }

    @Override
    public R deleteByUser(int userid) {
        return sysUserMapper.deleteById(userid)>0?R.ok("删除成功"):R.fail("删除失败");
    }

    @Override
    public R saveUserRole(int userid, Integer[] ids) {
        int r = 0;
        for (int i = 0; i < ids.length; i++) {
            r+=sysUserMapper.saveUserRoles(userid,ids[i]);
        }

        return r>0?R.ok("分配成功"):R.fail("分配失败");
    }

    @Override
    public R saveUser(SysUsers sysUsers) {
        return sysUserMapper.insert(sysUsers)>0?R.ok("新增成功"):R.fail("新增失败");
    }
}
