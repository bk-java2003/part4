package com.itqf.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itqf.entity.Cashier;
import com.itqf.entity.Pharmacy;
import com.itqf.entity.Register;
import com.itqf.mapper.CashierMapper;
import com.itqf.mapper.PharmacyMapper;
import com.itqf.mapper.RegisterMapper;
import com.itqf.service.CashierService;
import com.itqf.utils.R;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/4
 * @Time: 上午11:12
 */
@Service
public class CashierServiceImpl implements CashierService {
    @Resource
    private CashierMapper cashierMapper;

    @Resource
    private RegisterMapper registerMapper;

    @Resource
    private PharmacyMapper pharmacyMapper;


    @Override
    public R findChuByRegId(int regid, int page, int limit) {
        IPage iPage = new Page(page,limit);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("register_id",regid);

        iPage = cashierMapper.selectPage(iPage,queryWrapper);


        return R.ok().put("data",iPage.getRecords()).put("count",iPage.getTotal());
    }

    @Override
    public int findChu(int regid, String name) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("register_id",regid);
        queryWrapper.like("name",name);
        Cashier cashier = cashierMapper.selectOne(queryWrapper);
        return cashier==null?0:1;
    }

    @Override
    public R add(Cashier cashier) {

        return cashierMapper.insert(cashier)>0?R.ok("success"):R.fail("fail");
    }

    @Override
    public R update(Cashier cashier) {
        //1,cashier 数量要+num
        UpdateWrapper wrapper = new UpdateWrapper();
        wrapper.eq("register_id",cashier.getRegisterId());
        wrapper.eq("name",cashier.getName());
        Cashier oldCashier = cashierMapper.selectOne(wrapper);
        //药品数量=原来+新增的
        cashier.setNum(oldCashier.getNum()+cashier.getNum());
        //总价的计算
        cashier.setTotalPrice(oldCashier.getPrice()*cashier.getNum());
        int i = cashierMapper.update(cashier,wrapper);

        //2.pharmary  数量要-num
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("pharmacy_name",cashier.getName());
        Pharmacy pharmacy = pharmacyMapper.selectOne(queryWrapper);
        pharmacy.setDrugstoreNum(pharmacy.getDrugstoreNum()-cashier.getNum());
        i+=pharmacyMapper.update(pharmacy,queryWrapper);

        return i>1?R.ok("success"):R.fail("fail");
    }

    @Override
    public R del(int id) {
        return cashierMapper.deleteById(id)>0?R.ok("success"):R.fail("fail");
    }

    /**
     * 查询处方总的金额
     * @param regId
     * @return
     */
    @Override
    public double findTotalMoney(int regId) {
        return cashierMapper.findTotalMoney(regId);
    }

    @Override
    public R updateState(int id) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("register_id",id);
        //1.修改该处方的状态为缴费状态
        List<Cashier> cashiers = cashierMapper.selectList(queryWrapper);
        int i = 0;
        for (Cashier cashier : cashiers) {
            cashier.setState(1);//			'状态,0:未缴费，1:缴费 ，2:项目已做
             i+=cashierMapper.updateById(cashier);
        }

        //2.修改挂号的状态为2  已处理
        Register register = registerMapper.selectById(id);
        register.setState(2);
        i+=registerMapper.updateById(register);

        return i>0?R.ok("成功"):R.fail("失败");
    }

    @Override
    public R findNotPayDrug(int regid, int page, int limit) {
        IPage iPage = new Page(page,limit);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("register_id",regid);
        queryWrapper.eq("state",0);
        queryWrapper.eq("type",1);
        iPage = cashierMapper.selectPage(iPage,queryWrapper);

        return R.ok().put("data",iPage.getRecords()).put("count",iPage.getTotal());

    }
}
