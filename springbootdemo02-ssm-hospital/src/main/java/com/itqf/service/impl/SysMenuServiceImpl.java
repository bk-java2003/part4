package com.itqf.service.impl;

import com.alibaba.druid.util.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itqf.dto.MenuTreeNode;
import com.itqf.entity.SysMenu;
import com.itqf.mapper.SysMenuMapper;
import com.itqf.service.SysMenuService;
import com.itqf.utils.R;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/1
 * @Time: 下午3:17
 */
@Service //@Component  @Repository   @Controller   Spring构建对象
public class SysMenuServiceImpl implements SysMenuService {

    @Resource
    private SysMenuMapper sysMenuMapper;

    /**
     * 查询当前登录的用户能访问的目录和菜单
     * @param uid 用户编号
     *  门诊管理  目录 id 2
     *     用户挂号  菜单 id=22, pid(父目录)=2
     *     处方划价       id=22, pid(父目录)=2
     *     项目划价        id=22, pid(父目录)=2
     *  系统管理    id 3
     *     菜单管理  id=33, pid(父目录)=3
     *     用户管理  id=33, pid(父目录)=2
     * @return
     */
    @Override
    public List<MenuTreeNode> leftTreeMenu(int uid){
        List<MenuTreeNode> menuTreeNodes = new ArrayList<>();
        //1,用户能访问的目录
        List<SysMenu> dir = sysMenuMapper.findDirByUId(uid);
        //2,用户能访问的菜单
        for (SysMenu menu:dir){
            //menu--->MenuTreeNode
            MenuTreeNode node = new MenuTreeNode();
            node.setId(menu.getId());
            node.setParentId(menu.getPid());
            node.setCheckArr("0");
            node.setHref(menu.getHref());
            node.setIcon(menu.getIcon());
            node.setTitle(menu.getTitle());
            node.setTarget(menu.getTarget());

            menuTreeNodes.add(node);
            //得到目录的编号
            int id = menu.getId();
            //根据目录查询能访问的菜单
            List<SysMenu> child = sysMenuMapper.findMenuByUId(uid,id);
            //3，组装json（思想） [{[{}]},{}]
            // //json---children: [{},{}]     {}
            //    //java---List<MenuTreeNode>    对象|Map
            // bejson.com
            List<MenuTreeNode> childNode = new ArrayList<>();
            for (SysMenu m:child){
                MenuTreeNode c = new MenuTreeNode();
                c.setId(m.getId());
                c.setParentId(m.getPid());
                c.setCheckArr("0");
                c.setHref(m.getHref());
                c.setIcon(m.getIcon());
                c.setTitle(m.getTitle());
                c.setTarget(m.getTarget());
                childNode.add(c);
            }
            // "children": [{
            node.setChildren(childNode);
        }

        return menuTreeNodes;
    }


    @Override
    public R menuleftTreeJson() {

        List<MenuTreeNode> menuTreeNodes = new ArrayList<>();
        //1,用户能访问的目录
        List<SysMenu> dir = sysMenuMapper.selectList(null);
        //2,用户能访问的菜单
        for (SysMenu menu:dir) {
            //menu--->MenuTreeNode
            MenuTreeNode node = new MenuTreeNode();
            node.setId(menu.getId());
            node.setParentId(menu.getPid());
            node.setCheckArr("0");
            node.setHref(menu.getHref());
            node.setIcon(menu.getIcon());
            node.setTitle(menu.getTitle());
            node.setTarget(menu.getTarget());

            menuTreeNodes.add(node);
        }

        return R.ok().put("data",menuTreeNodes);
    }

    @Override
    public R menuRight(int page, int limit,SysMenu menu) {

        IPage iPage = new Page(page,limit);
        if (menu==null){
            iPage = sysMenuMapper.selectPage(iPage,null);
        }else{
            QueryWrapper queryWrapper = new QueryWrapper();
            if (!StringUtils.isEmpty(menu.getTitle())){
                queryWrapper.like("title",menu.getTitle());
            }
            if(menu.getId()!=null){
                queryWrapper.eq("id",menu.getId());
            }

            iPage = sysMenuMapper.selectPage(iPage,queryWrapper);
        }

        return R.ok().put("data",iPage.getRecords()).put("count",iPage.getTotal());
    }

    @Override
    public R updateMenu(SysMenu sysMenu) {
        return sysMenuMapper.updateById(sysMenu)>0?R.ok("修改成功"):R.fail("修改失败");
    }

    @Override
    public R addMenu(SysMenu sysMenu) {
        return sysMenuMapper.insert(sysMenu)>0?R.ok("修改成功"):R.fail("修改失败");
    }

    @Override
    public R deleteMenu(int id) {
        return sysMenuMapper.deleteById(id)>0?R.ok("修改成功"):R.fail("修改失败");
    }

    @Override
    public R checkMenuHasChildren(int id) {
        //系统管理 id=1
        //    菜单管理 id=11 pid=1

        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("pid",id);

        List<SysMenu> child = sysMenuMapper.selectList(queryWrapper);

        if (child!=null&&child.size()>0){
            return  R.ok();//有子菜单
        }

        return R.fail("没有子菜单");
    }
}
