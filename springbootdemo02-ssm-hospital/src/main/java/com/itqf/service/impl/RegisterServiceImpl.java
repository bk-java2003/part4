package com.itqf.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itqf.dto.RegisterInfoDTO;
import com.itqf.entity.Register;
import com.itqf.mapper.RegisterMapper;
import com.itqf.service.RegisterService;
import com.itqf.utils.R;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/3
 * @Time: 上午11:09
 */
@Service
public class RegisterServiceImpl  implements RegisterService {

    @Resource
    private RegisterMapper registerMapper;

    @Override
    public int check(String phone, String carid) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("phone",phone);
        List<Register> list = registerMapper.selectList(queryWrapper);
        if (list!=null&&list.size()>0){
            //该手机号已经挂过好了
            return 1;
        }
        queryWrapper.eq("carid",carid);
        list = registerMapper.selectList(queryWrapper);
        if (list!=null&&list.size()>0){
            // 该身份证号的患者已存在
            return 2;
        }

        return 3;
    }

    @Override
    public R saveRegister(Register register) {
        return registerMapper.insert(register)>0?R.ok("挂号成功"):R.fail("挂号失败");
    }

    @Override
    public List<RegisterInfoDTO> findAll(Integer cc,String name) {

        List<RegisterInfoDTO>  list = registerMapper.findAll( cc, name);

        //List<RegisterInfoDTO>  newList = new ArrayList<>();
        for (RegisterInfoDTO registerInfoDTO : list) {
            String c = registerInfoDTO.getCarid();
            //212212121213232
            String  old = c.substring(11);
            c = c.substring(0,12);
            for(int i=0;i<old.length();i++){
                c+="*";
            }

            registerInfoDTO.setCarid(c);
            System.out.println(c);

        }
        return list;
    }

    /*
        page：第几页
        limit ：每页多少条
     */
    @Override
    public R findByPage(int page, int limit) {
        int  index = (page-1)*limit;

        List<RegisterInfoDTO> list =  registerMapper.findByPage(index,limit);
        int total  = registerMapper.findTotalCount();
        //layui json的格式
        //{"code":0,msg:"",data:[{},{}],count:100}
        return  R.ok("").put("data",list).put("count",total);

    }

    @Override
    public String findReason(int regId){
        Register register = registerMapper.selectById(regId);
        if (register!=null){
            return  register.getZhuan();//
        }
        return null;
    }

    @Override
    public R addReason(int regId, String bing) {
        Register register = registerMapper.selectById(regId);
        if (register!=null){
              register.setZhuan(bing);
              int i = registerMapper.updateById(register);
              return i>0?R.ok("success"):R.fail("fail");
        }

        return R.fail();
    }


    @Override
    public List<Double> findLineData(int year) {
        List<Double> newList = new ArrayList<>();
        List<Double> list = registerMapper.findLineData(year);
        for (Double aDouble : list) {
            if (aDouble==null){
                newList.add(0.0);
            }
            newList.add(aDouble);
        }
        return newList;
    }

    @Override
    public List<Map<String, Object>> findYearPieData() {
        return registerMapper.findYearPieData();
    }

    @Override
    public List<Map<String, Object>> findCurrentPieData(String current) {
        return registerMapper.findCurrentPieData(current);
    }

    @Override
    public R findDoctorData(int page, int limit) {

        List<Map<String,Object>> list = registerMapper.findDoctorData((page-1)*page,limit);
        int total = registerMapper.findDoctorDataTotalCount();

        return R.ok("").put("count",total).put("data",list);
    }
}
