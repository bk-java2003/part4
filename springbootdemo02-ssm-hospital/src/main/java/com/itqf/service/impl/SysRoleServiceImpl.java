package com.itqf.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itqf.entity.SysRole;
import com.itqf.mapper.SysRoleMapper;
import com.itqf.mapper.SysUserMapper;
import com.itqf.service.SysRoleService;
import com.itqf.utils.R;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/2
 * @Time: 上午11:24
 */
@Service
public class SysRoleServiceImpl implements SysRoleService {

    @Resource
    private SysRoleMapper sysRoleMapper;

    @Resource
    private SysUserMapper sysUserMapper;

    /**
     * 查询用户不具有的角色列表
     *
     * @param userid
     * @return
     */
    @Override
    public R findRoleList(int userid) {
        //1,先查用户具有的角色
        List<Integer> ids = sysUserMapper.findRoleIdsByUid(userid);
        //2.再查询用户不具有的角色
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.notIn("roleid",ids);

        List<SysRole> list= sysRoleMapper.selectList(queryWrapper);
        return R.ok().put("data",list).put("count",list.size());
    }
}
