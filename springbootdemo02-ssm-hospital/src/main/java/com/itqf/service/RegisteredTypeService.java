package com.itqf.service;

import com.itqf.entity.RegisteredType;

import java.util.List;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/3
 * @Time: 上午10:08
 */
public interface RegisteredTypeService  {

    public List<RegisteredType> findAllRegType();

    public   Double   findMoney(int id);
}
