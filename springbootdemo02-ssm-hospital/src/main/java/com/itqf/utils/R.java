package com.itqf.utils;

import java.util.HashMap;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/1
 * @Time: 上午11:07
 */
public class R extends HashMap {
    private int code;//状态码  1 失败  0成功
    private  String msg;

    public R(){}
    public R(int  code){
        super.put("code",code);
    }
    public R(int code,String msg){
        super.put("code",code);
        super.put("msg",msg);
    }

    public  static R ok(){
        return  new R(0);
    }
    public  static R ok(String msg){
        return  new R(0,msg);
    }

    public  static R fail(){
        return  new R(1);
    }
    public  static R fail(String msg){
        return  new R(1,msg);
    }

    public  R  put(String k,Object v){
        super.put(k,v);

        return  this;
    }



}
