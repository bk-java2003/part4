package com.itqf;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.itqf.mapper")
public class Springbootdemo02SsmHospitalApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springbootdemo02SsmHospitalApplication.class, args);
    }

}
