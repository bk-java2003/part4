package com.itqf.dto;

import com.itqf.entity.Pharmacy;
import lombok.Data;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/4
 * @Time: 上午9:54
 */
@Data
public class PharmacyDTO extends Pharmacy {

    private  String  unitName;

}
