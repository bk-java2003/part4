package com.itqf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itqf.dto.PharmacyDTO;
import com.itqf.entity.Pharmacy;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/4
 * @Time: 上午9:46
 */
public interface PharmacyMapper extends BaseMapper<Pharmacy> {
    //@Param("index")    xml  index 常用
    //什么都不加  xml  arg0  arg1
    // 什么都不加  xml param1  param2
    public List<PharmacyDTO> findDrug(@Param("startIndex") int index, @Param("pageSize") int limit);
    public   int  findTotalCount();
}
