package com.itqf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itqf.entity.Doctor;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/3
 * @Time: 上午10:05
 */
public interface DoctorMapper  extends BaseMapper<Doctor> {

    public List<Doctor> findDoctorByColumn(@Param("column") String column);

}
