package com.itqf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itqf.entity.SysRole;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/2
 * @Time: 上午11:25
 */
public interface SysRoleMapper  extends BaseMapper<SysRole> {
}
