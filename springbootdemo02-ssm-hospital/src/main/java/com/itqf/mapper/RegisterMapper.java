package com.itqf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itqf.dto.RegisterInfoDTO;
import com.itqf.entity.Register;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/3
 * @Time: 上午11:09
 */
public interface RegisterMapper  extends BaseMapper<Register> {

    //arg0  arg1
    public List<RegisterInfoDTO> findAll(@Param("cc") Integer cc, @Param("name") String name);


    public List<RegisterInfoDTO> findByPage(@Param("startIndex") Integer startIndex, @Param("limit") int limit);

    public  int findTotalCount();

    public List<Double> findLineData(@Param("year") int year);

    public List<Map<String,Object>> findYearPieData();

    public List<Map<String,Object>> findCurrentPieData(@Param("current") String current);


    public List<Map<String,Object>> findDoctorData(@Param("startIndex") int startIndex, @Param("pageSize") int pageSize);
    public  int findDoctorDataTotalCount();


}
