package com.itqf.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * @Description:  shiro配置类    加载spring-shiro的配置文件
 *                  1） java类的方式替代
 *                   2）加载xml
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/9
 * @Time: 上午11:46
 */
@Configuration
@ImportResource(locations = "classpath:spring/spring-shiro.xml")
public class ShiroConfig {
}
