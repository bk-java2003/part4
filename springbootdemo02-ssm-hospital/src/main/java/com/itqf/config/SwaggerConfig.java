package com.itqf.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/24
 * @Time: 下午4:41
 */
//@Component
@Configuration  //spring配置文件 <beans>
@EnableSwagger2  //开启swagger
public class SwaggerConfig {

    @Bean
    public Docket getDocket(){

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false)
                .select()
                //.paths(Predicates.or(PathSelectors.regex("/api2/.*")))//  过滤  该路径下的请求生成RESTful api
                .build();
    }

    private ApiInfo apiInfo(){
        //1  2
        return new ApiInfoBuilder()
                .title("北京国际医院后台管理系统")//大标题
                .description("北京国际医院后台管理系统")//详细描述
                .version("2.0")//版本
                .contact(new Contact("java", "http://itqf.com", "123456@qq.com"))//作者
                .license("The Apache License, Version 2.0")//许可证信息
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")//许可证地址
                .build();

    }

}
