package com.itqf.controller;

import com.itqf.dto.MenuTreeNode;
import com.itqf.entity.SysUsers;
import com.itqf.service.SysMenuService;
import com.itqf.service.SysRoleService;
import com.itqf.service.SysUserService;
import com.itqf.utils.R;
import com.itqf.utils.ShiroUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;


/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/1
 * @Time: 上午11:06
 */
@RestController
@Api("用户管理模块")
public class SysUserController {

    @Resource
    private SysMenuService sysMenuService;
    @Resource
    private SysUserService sysUserService;

    @Resource
    private SysRoleService sysRoleService;


    Logger logger = LoggerFactory.getLogger(SysUserController.class);


    //@RequestMapping("/sel/doLogin")
    @RequestMapping("/sel/doLogin")//
    @ApiOperation("登录")
    public R login(@ApiParam(name = "loginname",value = "用户名",defaultValue = "admin")@RequestParam("loginname") String loginname, @ApiParam(name = "pwd",value = "密码",defaultValue = "123456")@RequestParam("pwd") String pwd){

        try {
            //1,构建subject
            Subject subject = SecurityUtils.getSubject();
            //2.构建token
            UsernamePasswordToken token = new UsernamePasswordToken(loginname,pwd);
            //3.调用login
            subject.login(token);
            //shiro内部会调用自定义realm中的认证方法
        } catch (UnknownAccountException e) {
            e.printStackTrace();
            return  R.fail("登录失败："+e.getMessage());
        }catch (IncorrectCredentialsException e) {
            e.printStackTrace();
            return  R.fail("登录失败,密码错误");
        }

        return  R.ok();
    }

    /**
     * 返回用户能访问的菜单
     * 思考：哪些表
     * sys_user sys_user_role  sys_role sys_role_menu   sys_menu
     * sql语句??
     * @return
     */
    @RequestMapping("/sel/toTreeLoad")
    @ApiOperation("用户能看到的左侧菜单")
    public List<MenuTreeNode> toTreeLoad(){
    //如何得到当前登录的用户编号
        //1.session
        //2.shiro          this.principals = new SimplePrincipalCollection(principal, realmName);

        int uid = ShiroUtils.getUid();

     return  sysMenuService.leftTreeMenu(uid);
    }

    @GetMapping("/user/selectAllUser")
    @ApiOperation("分页查询所有用户")
    public   R   selectAllUser(int page, int limit, SysUsers sysUsers){

        return  sysUserService.findAllUser(page,limit,sysUsers);
    }

    @RequestMapping("/user/updateUser")
    @ApiOperation("修改能看到的左侧菜单")
    public    R  updateUser(SysUsers sysUsers){
        return sysUserService.updateUser(sysUsers);
    }

    /**
     * 重置用户密码
     * @param userid
     * @return
     */
    @RequestMapping("/user/resetUserPwd")
    public   R  resetUserPwd(int userid){
        //1,得到原来的值
        SysUsers sysUsers = sysUserService.findById(userid);
        //2,初始密码123456
        String salt = ShiroUtils.getSalt();//6
        System.out.println("---->"+salt);
        logger.info("【salt】"+salt);
        sysUsers.setSalt(salt);
        sysUsers.setPwd(ShiroUtils.getPwd("123456",salt));

        return  sysUserService.updateUser(sysUsers);
    }

    @RequestMapping("/user/deleteUser")//user/deleteUser
    public   R  deleteUser(int userid){
        return  sysUserService.deleteByUser(userid);
    }

    /**
     * 查询出用户不具有的所有的角色
     * /user/initUserRole?userid='+data.userid
     */
    @RequestMapping("/user/initUserRole")
    public  R  initUserRole(int  userid){

        return  sysRoleService.findRoleList(userid);
    }

    /**
     * 给用户分配角色
     * @return
     */
    @RequestMapping("/user/saveUserRole")
    public  R  saveUserRole(int userid,Integer[] ids){

        logger.info("【userid】"+userid+"【roleids】"+ids);

        return  sysUserService.saveUserRole(userid,ids);

    }

    /**
     * 管理员分配账号
     * 设置初始密码
     * @param sysUsers
     * @return
     */
    @RequestMapping("/user/addUser")
    @ApiOperation("新增用户")
    public   R  saveUser( SysUsers sysUsers){
         String salt  = ShiroUtils.getSalt();
         String pwd = ShiroUtils.getPwd("123456",salt);
         sysUsers.setSalt(salt);
         sysUsers.setPwd(pwd);
         return  sysUserService.saveUser(sysUsers);
    }

}
