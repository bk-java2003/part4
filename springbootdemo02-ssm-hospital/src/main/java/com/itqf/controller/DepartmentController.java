package com.itqf.controller;

import com.itqf.entity.Departments;
import com.itqf.service.DepartmentsService;
import com.itqf.utils.R;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Description: 上传代码
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/5
 * @Time: 下午4:38
 */
@RestController
public class DepartmentController {


    @Resource
    private DepartmentsService departmentsService;

    @RequestMapping("/department/departmentList")
    public R  departmentList(int page,int limit ,Departments departments){
        System.out.println(page+"--"+limit);
        return  departmentsService.findByPage(page,limit,departments);
    }


}


