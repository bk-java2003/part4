package com.itqf.controller;

import com.itqf.service.RegisterService;
import com.itqf.utils.R;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/5
 * @Time: 上午10:20
 */
@RestController
public class EchartsController {


    @Resource
    private RegisterService registerService;

    //门诊月度统计  折线图
    //  data: [150, 230, 224, 218, 135, 147, 260],
    //  []-->java  List|Array
    //{}--->java   对象(Student,Map,JsonObject)
    @RequestMapping("/finance/reportYearFinance")
    public List<Double> findLineData(int year){//2020  2021  2019
        //门诊：  统计这一年每个月入账多少钱？？
        return  registerService.findLineData(year);
    }

    //门诊年度统计  饼图
    @RequestMapping("/finance/reportYearBingFinance")
    // data: [{value: 1048, name: '搜索引擎'},{}]
    //List<Map>
    public  List<Map<String,Object>>   findYearPieData(){

        return  registerService.findYearPieData();
    }

    //门诊当天药物统计
    @RequestMapping("/finance/currentFinance")
    public List<Map<String,Object>>  findCurrentPieData(String current){


        return  registerService.findCurrentPieData(current);
    }


    //医生统计对比
    @RequestMapping("/finance/doctorDuibi")
    //layui table  {code:0,msg:"",data:[{}],count:22}
    public R findDoctorData(int page,int limit){
        System.out.println("测试");
        return  registerService.findDoctorData(page,limit);
    }


}
