package com.itqf.controller;

import com.itqf.entity.Departments;
import com.itqf.entity.Doctor;
import com.itqf.entity.Register;
import com.itqf.entity.RegisteredType;
import com.itqf.service.DepartmentsService;
import com.itqf.service.DoctorService;
import com.itqf.service.RegisterService;
import com.itqf.service.RegisteredTypeService;
import com.itqf.utils.R;
import com.itqf.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @Description: 挂号
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/2/3
 * @Time: 上午9:59
 */
//@Controller
@RestController
public class RegisterController {

    @Resource
    private DepartmentsService departmentsService;
    @Autowired
    private RegisteredTypeService registeredTypeService;
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private RegisterService registerService;



    /**
     * 查询科室列表
     * @return
     */
    @RequestMapping("/cao/seldep")
    public List<Departments> selectDepartments(){
        return  departmentsService.findAllDepartments();
    }

    /**
     * 查询挂号类型列表
     * @return
     */
    @RequestMapping("/cao/selreg")
    public List<RegisteredType>  selectRegisterType(){
            return  registeredTypeService.findAllRegType();
    }

    /**
     *  url: '/cao/seldoctor',
     *  data:{"registeredid":registeredId,"departmentId":departmentId},
     *
     */
    /**
     * 查询挂号类型列表
     * @return
     */
    @RequestMapping("/cao/seldoctor")
    public List<Doctor>  selectDoctor(int registeredId, int departmentId){
        return  doctorService.findDoctor(registeredId,departmentId);
    }


    //校验手机号和身份证
    @RequestMapping("/cao/phone")
    //"phone":phone,"carid":carid
    public   Object checkPhoneAndCard(String phone,String carid){
        return registerService.check(phone,carid);
    }

    //挂号  保证一份用户的挂号信息到挂号表
    @RequestMapping("/cao/addReg")
    public R saveRegister(Register register){
        register.setTime(new Date());
        register.setState(1);//已经挂号，未处理
        register.setUserId(ShiroUtils.getUid());
        return   registerService.saveRegister(register);
    }

    @RequestMapping("/cao/selRegMoney")
    public  Double findMoney(int registeredId){
        return  registeredTypeService.findMoney(registeredId);
    }

    // $.get("/cao/index?cc="+cc+"&name="+name,function(r){
    @RequestMapping("/cao/index")
    public   List  findRegister(Integer cc,String name){
        System.out.println(cc+"---"+name);

        return registerService.findAll(cc,name);
    }

}
