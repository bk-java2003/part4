package com.itqf.demo06_confirm;

import com.itqf.utils.ConnectionUtils;
import com.itqf.utils.SysConstant;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConfirmListener;
import com.rabbitmq.client.Connection;

import java.io.IOException;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/12
 * @Time: 上午10:38
 */
public class TopicSend {

    public  static  void  main(String[]args) throws  Exception{
        Connection connection = ConnectionUtils.getConnection();
        //通道 Channel
        Channel channel = connection.createChannel();
        //声明一个exchange
        channel.exchangeDeclare(SysConstant.TOPIC_EXCHANGE_NAME, BuiltinExchangeType.TOPIC);

        //发布消息
        channel.confirmSelect();//开启confirm机制

        //异步 确认
        channel.addConfirmListener(new ConfirmListener() {

            @Override
            public void handleAck(long deliveryTag, boolean multiple) throws IOException {
                System.out.println("发送成功！！"+deliveryTag+"是否批量"+multiple);
            }

            @Override
            public void handleNack(long deliveryTag, boolean multiple) throws IOException {
                System.out.println("发送失败！！"+deliveryTag+"是否批量"+multiple);

            }
        });


        //消息正确的发出去会得到一个状态
        String msg = "hello---sys.add.user";
        channel.basicPublish(SysConstant.TOPIC_EXCHANGE_NAME,"sys.add.user",null,msg.getBytes());

         msg = "hello---add";
        channel.basicPublish(SysConstant.TOPIC_EXCHANGE_NAME,"add",null,msg.getBytes());
        //同步
//        if (channel.waitForConfirms()){
//        //Wait until all messages published
//            System.out.println("发送成功！！");
//        }else{
//            System.out.println("发送失败！！");
//        }

       // channel.waitForConfirmsOrDie();//批量确认




        channel.close();
        connection.close();


    }


}
