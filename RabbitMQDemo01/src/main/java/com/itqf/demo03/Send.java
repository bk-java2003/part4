package com.itqf.demo03;

import com.itqf.utils.ConnectionUtils;
import com.itqf.utils.SysConstant;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/12
 * @Time: 上午10:38
 */
public class Send {

    public  static  void  main(String[]args) throws  Exception{
        Connection connection = ConnectionUtils.getConnection();
        //通道 Channel
        Channel channel = connection.createChannel();
        //声明一个exchange
        channel.exchangeDeclare(SysConstant.FANOUT_EXCHANGE_NAME, BuiltinExchangeType.FANOUT);

        //发布消息
        for (int i = 0; i < 100; i++) {
            String msg = "hello---"+i;
            channel.basicPublish(SysConstant.FANOUT_EXCHANGE_NAME,"",null,msg.getBytes());
        }

        System.out.println("发送完毕！！");

        channel.close();
        connection.close();


    }


}
