package com.itqf.demo03;

import com.itqf.utils.ConnectionUtils;
import com.itqf.utils.SysConstant;
import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/12
 * @Time: 上午10:44
 */
public class FanoutRec1 {
    public  static  void  main(String[]args) throws Exception{
        Connection connection = ConnectionUtils.getConnection();
        //通道 Channel
        Channel channel = connection.createChannel();
        //声明一个exchange
        channel.exchangeDeclare(SysConstant.FANOUT_EXCHANGE_NAME, BuiltinExchangeType.FANOUT);
        //声明队列
        channel.queueDeclare(SysConstant.FANOUT_QUEUE_NAME1,false,false,false,null);
        //绑定交换机和队列
        channel.queueBind(SysConstant.FANOUT_QUEUE_NAME1,SysConstant.FANOUT_EXCHANGE_NAME,"");

        DefaultConsumer defaultConsumer = new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("消费者1："+new String (body));

                //手动应答
                channel.basicAck(envelope.getDeliveryTag(),false);
            }
        };

        //
        channel.basicConsume(SysConstant.FANOUT_QUEUE_NAME1,false,defaultConsumer);





    }
}
