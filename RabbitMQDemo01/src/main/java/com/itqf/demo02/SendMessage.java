package com.itqf.demo02;

import com.itqf.utils.ConnectionUtils;
import com.itqf.utils.SysConstant;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/12
 * @Time: 上午9:21
 */
public class SendMessage {

    public  static  void  main(String[]args) throws  Exception{
        Connection  connection = ConnectionUtils.getConnection();
        //通道 Channel
        Channel channel = connection.createChannel();
        //队列
        //(String queue, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments
        channel.queueDeclare(SysConstant.WORK_QUEUE_NAME,false,false,false,null);
        //发消息
        for (int i = 0; i < 100; i++) {
            String msg = "hello--"+i;
            //String exchange, String routingKey, boolean mandatory, BasicProperties props, byte[] body
            channel.basicPublish("",SysConstant.WORK_QUEUE_NAME,true,null,msg.getBytes());

        }

        System.out.println("消息发送成功");


    }



}
