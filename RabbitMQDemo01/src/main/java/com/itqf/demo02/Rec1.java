package com.itqf.demo02;

import com.itqf.utils.ConnectionUtils;
import com.itqf.utils.SysConstant;
import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/12
 * @Time: 上午9:33
 */
public class Rec1 {
    public  static  void  main(String[]args) throws  Exception{

        Connection connection = ConnectionUtils.getConnection();
        //通道 Channel
        Channel channel = connection.createChannel();
        //队列
        //(String queue, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments
        channel.queueDeclare(SysConstant.WORK_QUEUE_NAME,false,false,false,null);

        channel.basicQos(1);
        //消费者
        DefaultConsumer defaultConsumer = new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
               //接收到消息
                System.out.println("消费者1："+new String(body));
                //业务处理
                //持久化到db
                //手动应答
                channel.basicAck(envelope.getDeliveryTag(),false);
            }
        };
        //String queue, Consumer callback
        //basicConsume(queue, false, callback);
        channel.basicConsume(SysConstant.WORK_QUEUE_NAME,defaultConsumer);

        System.in.read();

        channel.close();
        connection.close();



    }


}
