package com.itqf.demo05;

import com.itqf.utils.ConnectionUtils;
import com.itqf.utils.SysConstant;
import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/12
 * @Time: 上午10:44
 */
public class TopicRec2 {
    public  static  void  main(String[]args) throws Exception{
        Connection connection = ConnectionUtils.getConnection();
        //通道 Channel
        Channel channel = connection.createChannel();
        //声明一个exchange
        channel.exchangeDeclare(SysConstant.TOPIC_EXCHANGE_NAME, BuiltinExchangeType.TOPIC);
        //声明队列
        channel.queueDeclare(SysConstant.TOPIC_QUEUE_NAME2,false,false,false,null);
        //绑定交换机和队列
        channel.queueBind(SysConstant.TOPIC_QUEUE_NAME2,SysConstant.TOPIC_EXCHANGE_NAME,"*.update.*");
        channel.queueBind(SysConstant.TOPIC_QUEUE_NAME2,SysConstant.TOPIC_EXCHANGE_NAME,"#.delete.*");
        channel.queueBind(SysConstant.TOPIC_QUEUE_NAME2,SysConstant.TOPIC_EXCHANGE_NAME,"*.add.*");


        DefaultConsumer defaultConsumer = new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("消费者2："+new String (body));

                //手动应答
                channel.basicAck(envelope.getDeliveryTag(),false);
            }
        };

        //
        channel.basicConsume(SysConstant.TOPIC_QUEUE_NAME2,false,defaultConsumer);

        


    }
}
