package com.itqf.demo05;

import com.itqf.utils.ConnectionUtils;
import com.itqf.utils.SysConstant;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/12
 * @Time: 上午10:38
 */
public class TopicSend {

    public  static  void  main(String[]args) throws  Exception{
        Connection connection = ConnectionUtils.getConnection();
        //通道 Channel
        Channel channel = connection.createChannel();
        //声明一个exchange
        channel.exchangeDeclare(SysConstant.TOPIC_EXCHANGE_NAME, BuiltinExchangeType.TOPIC);

        //发布消息

        String msg = "hello---sys.add.user";
        channel.basicPublish(SysConstant.TOPIC_EXCHANGE_NAME,"sys.add.user",null,msg.getBytes());

         msg = "hello---add";
        channel.basicPublish(SysConstant.TOPIC_EXCHANGE_NAME,"add",null,msg.getBytes());

        msg = "hello---update";
        channel.basicPublish(SysConstant.TOPIC_EXCHANGE_NAME,"sys.update.user",null,msg.getBytes());

         msg = "hello---delete";
        channel.basicPublish(SysConstant.TOPIC_EXCHANGE_NAME,"sys.delete.user",null,msg.getBytes());

         msg = "hello---find";
        channel.basicPublish(SysConstant.TOPIC_EXCHANGE_NAME,"sys.find.user.sss",null,msg.getBytes());


        System.out.println("发送完毕！！");

        channel.close();
        connection.close();


    }


}
