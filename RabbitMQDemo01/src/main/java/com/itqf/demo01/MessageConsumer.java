package com.itqf.demo01;

import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/11
 * @Time: 下午5:44
 */
public class MessageConsumer {

    public  static  void  main(String[]args) throws  Exception{
        //1,创建ConnectionFactory
        ConnectionFactory factory = new ConnectionFactory();
        factory.setVirtualHost("/qf");
        factory.setUsername("qf");
        factory.setPassword("qf");
        factory.setHost("192.168.82.168");
        factory.setPort(5672);//监听消息
        //15672 rabbitmq的后台管理系统的端口
        //2.得到Connection
        Connection connection = factory.newConnection();
        //3.得到通道
        Channel channel = connection.createChannel();

        //4.声明队列
        //参数1 队列名,
        //参数2 durable： 是否持久化
        //参数3 exclusive：是否排外的，有两个作用，一：当连接关闭时connection.close()该队列是否会自动删除；一般等于true的话用于一个队列只能有一个消费者来消费的场景
        //autoDelete：是否自动删除，当最后一个消费者断开连接之后队列是否自动被删除，可以通过RabbitMQ Management，查看某个队列的消费者数量，当consumers = 0时队列就会自动删除
        channel.queueDeclare("hello",false,false,false,null);

        //5.接收消息
        //声明消费者
        DefaultConsumer consumer = new DefaultConsumer(channel){
            /**
             * 接收到消息  调用
             * @param consumerTag
             * @param envelope
             * @param properties
             * @param body
             * @throws IOException
             */
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("接收到消息："+new String(body));
            }
        };

        //6.声明消费的队列
        //String queue, Consumer callback
        channel.basicConsume("hello",true,consumer);

        System.in.read();

        channel.close();
        connection.close();

    }


}
