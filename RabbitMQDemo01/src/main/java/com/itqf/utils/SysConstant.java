package com.itqf.utils;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/12
 * @Time: 上午9:28
 */
public class SysConstant {

    //一条消息可以有多个消费者尝试消费，但是最终只能有一个消费者消费该条消息
    public  static  final String   WORK_QUEUE_NAME="work-queue";

    //fanout   发布订阅模式
    //一个exchange  两个queue
    //两个消费者分别绑定到不同的队列
    //一条消息能被多个消费者消费到
    public  static  final String   FANOUT_EXCHANGE_NAME="fanout-exchange";
    public  static  final String   FANOUT_QUEUE_NAME1="fanout-queue1";
    public  static  final String   FANOUT_QUEUE_NAME2="fanout-queue2";


    //direct   路由（定向）模式
    //一个exchange  两个queue
    //两个消费者分别绑定到不同的队列
    //一条消息能被订阅了该消息的消费者消费到
    //String exchange, String routingKey
    public  static  final String   DIRECT_EXCHANGE_NAME="direct-exchange";
    public  static  final String   DIRECT_QUEUE_NAME1="direct-queue1";
    public  static  final String   DIRECT_QUEUE_NAME2="direct-queue2";



    //topic   通配符模式
    //支持通配符  *:一个单词   # 0到多个单词
    //一个exchange  两个queue
    //两个消费者分别绑定到不同的队列
    //一条消息能被订阅了该消息的消费者消费到
    //String exchange, String routingKey
    public  static  final String   TOPIC_EXCHANGE_NAME="topic-exchange";
    public  static  final String   TOPIC_QUEUE_NAME1="topic-queue1";
    public  static  final String   TOPIC_QUEUE_NAME2="topic-queue2";



}
