package com.itqf.utils;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/12
 * @Time: 上午9:22
 */
public class ConnectionUtils {

    public static Connection getConnection(){
        //1,ConnectionFactory
        ConnectionFactory c = new ConnectionFactory();
        c.setPort(5672);
        c.setHost("192.168.82.168");
        c.setVirtualHost("/qf");
        c.setUsername("qf");
        c.setPassword("qf");

        //2.得到connection
        try {
            return  c.newConnection();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        return  null;

    }

}
