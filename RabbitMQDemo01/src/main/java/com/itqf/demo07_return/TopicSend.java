package com.itqf.demo07_return;

import com.itqf.utils.ConnectionUtils;
import com.itqf.utils.SysConstant;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * @Description:
 * @Company: 千锋互联
 * @Author: 李丽婷
 * @Date: 2021/3/12
 * @Time: 上午10:38
 */
public class TopicSend {

    public  static  void  main(String[]args) throws  Exception{
        Connection connection = ConnectionUtils.getConnection();
        //通道 Channel
        Channel channel = connection.createChannel();
        //声明一个exchange
        channel.exchangeDeclare(SysConstant.TOPIC_EXCHANGE_NAME, BuiltinExchangeType.TOPIC);

        //发布消息

        String msg = "hello---sys.add.user";
        //mandatory:true   开启消息回退
        channel.basicPublish(SysConstant.TOPIC_EXCHANGE_NAME,"sys.add.user",true,null,msg.getBytes());
        channel.addReturnListener(new ReturnListener() {
            @Override
            public void handleReturn(int replyCode, String replyText, String exchange, String routingKey, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("replyCode:"+replyCode);
                System.out.println("replyText:"+replyText+",exchange:"+exchange+",routingKey:"+routingKey+",退回的消息："+new String(body));

                //return_msg 表中
                //定时任务
                //重发
            }
        });


        TimeUnit.SECONDS.sleep(3);
        //System.out.println("发送完毕！！");

        channel.close();
        connection.close();


    }


}
